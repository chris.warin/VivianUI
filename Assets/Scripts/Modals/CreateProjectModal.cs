using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.chwar.xrui;
using SFB;
using UnityEngine.UIElements;

namespace Modals
{
    public class CreateProjectModal : Modal
    {
        // Main page UXML elements
        private TextField _name;
        private TextField _directory;
        private TextField _models;
        private Button _browseDirectoryBtn;
        private Button _chooseBtn;
        private Toggle _subdirToggle;

        // Import method page UXML elements
        private Button _fileBtn;
        private Button _urlBtn;
        private Button _cloudBtn;

        // Browse files page UXML elements
        private TextField _files;
        private Button _browseFilesBtn;

        private List<string> _modelsPathList = new();
        private bool _manualFilePath;

        protected override IEnumerator Start()
        {
            yield return base.Start();
            XRUIModal.ValidateButton.SetEnabled(false);
            // Instantiate the main content in the container
            OpenMainPage();
        }

        private void OpenMainPage()
        {
            XRUIModal.UpdateModalFlow("CreateProjectModal_Main", "MainContainer", () =>
            {
                // Map button handlers
                _browseDirectoryBtn = UIDocument.rootVisualElement.Q<Button>("BrowseDirectory");
                _browseDirectoryBtn.clicked += OpenDirectoryExplorer;
                _chooseBtn = UIDocument.rootVisualElement.Q<Button>("Choose");
                _chooseBtn.clicked += OpenImportPage;
                _subdirToggle = UIDocument.rootVisualElement.Q<Toggle>("Subdirectory");

                // Get text fields
                _name = UIDocument.rootVisualElement.Q<TextField>("Name");
                _directory = UIDocument.rootVisualElement.Q<TextField>("Directory");
                _models = UIDocument.rootVisualElement.Q<TextField>("Models");

                // Register callbacks to check that form is filled
                XRUIModal.SetRequiredFields(_name, _directory, _models);
            });

            GetModelsPaths(_models);
            XRUIModal.ModalTitle.text = "Create a new project";
            XRUIModal.CancelButton.text = "Cancel";
            XRUIModal.ValidateButton.text = "Finish";
            XRUIModal.ValidateButton.visible = true;
            XRUIModal.SetValidateButtonAction(CreateProject);
            XRUIModal.SetCancelButtonAction(() =>
            {
                Destroy(XRUIModal.gameObject);
                _manualFilePath = false;
            });
            XRUIModal.SetButtonsPlacement(Justify.Center);
        }

        private void OpenImportPage()
        {
            XRUIModal.UpdateModalFlow("CreateProjectModal_ImportMethod", "MainContainer", () =>
            {
                _fileBtn = UIDocument.rootVisualElement.Q<Button>("File");
                _fileBtn.clicked += () => OpenBrowseFilesPage(false);
                _urlBtn = UIDocument.rootVisualElement.Q<Button>("URL");
                _urlBtn.clicked += () => OpenBrowseFilesPage(true);
                _cloudBtn = UIDocument.rootVisualElement.Q<Button>("Cloud");
                _cloudBtn.SetEnabled(false);
            });

            XRUIModal.ModalTitle.text = "How would you like to import your model(s)?";
            XRUIModal.CancelButton.text = "Back";
            XRUIModal.ValidateButton.visible = false;
            XRUIModal.SetCancelButtonAction(OpenMainPage);
            XRUIModal.SetButtonsPlacement(Justify.FlexStart);
        }

        private void OpenBrowseFilesPage(bool bUrl)
        {
            XRUIModal.UpdateModalFlow("CreateProjectModal_BrowseFiles", "MainContainer", () =>
            {
                _files = UIDocument.rootVisualElement.Q<TextField>("Files");
                _files.RegisterCallback<KeyDownEvent>(c => _manualFilePath = true);
                _browseFilesBtn = UIDocument.rootVisualElement.Q<Button>("BrowseModels");
                _browseFilesBtn.clicked += OpenFileExplorer;
                XRUIModal.SetRequiredFields(_files);
            });

            _browseFilesBtn.style.display = bUrl ? DisplayStyle.None : DisplayStyle.Flex;
            _files.label = bUrl ? "URL" : "File(s) location";

            XRUIModal.ModalTitle.text = bUrl ? "Import from URL" : "Browse Files";
            XRUIModal.CancelButton.text = "Back";
            XRUIModal.ValidateButton.text = "Next";
            XRUIModal.ValidateButton.visible = true;
            XRUIModal.SetValidateButtonAction(bUrl ? CheckUrl : CheckPath);
            XRUIModal.SetCancelButtonAction(OpenImportPage);
            XRUIModal.SetButtonsPlacement(Justify.Center);
        }

        private void CheckPath()
        {
            // If user wrote a path manually instead of the file browser
            if (_manualFilePath)
            {
                _modelsPathList.Clear();
                _modelsPathList.Add(_files.value);
            }
            GetModelsPaths(_files);
            OpenMainPage();
        }

        private void CheckUrl()
        {
            bool result = Uri.TryCreate(_files.value, UriKind.Absolute, out var uriResult)
                          && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (result)
            {
                _modelsPathList.Add(_files.value);
                OpenMainPage();
            }
            else
            {
                XRUI.Instance.ShowAlert(XRUI.AlertType.Danger, "Error", "This URL is invalid.");
                XRUIModal.SetFieldError(_files);
            }
        }

        private void CreateProject()
        {
            VivianUI.Instance.CreateProject(_name.text, _directory.text, _modelsPathList, _subdirToggle.value);
            var menu = FindObjectOfType<Menu>();
            menu.Animate(false);
            menu.AllowCollapsing(true);
            Destroy(XRUIModal.gameObject);
        }

        private void OpenDirectoryExplorer()
        {
            StandaloneFileBrowser.OpenFolderPanelAsync("Open File", "", false, paths =>
            {
                _directory.value = paths[0] != string.Empty ? paths[0] : _directory.value;
            });
        }        
        
        private void OpenFileExplorer()
        {
            var extensions = new [] {
                new ExtensionFilter("Model Files", "fbx", "obj", "blend" ),
            };
            var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, true);
            if (!paths[0].Equals(string.Empty))
            {
                _modelsPathList = paths.ToList();
                // Fill the text field with the selected file path
                GetModelsPaths(_files);
                _manualFilePath = false;
            }
        }

        private void GetModelsPaths(TextField tf)
        {
            List<string> modelNames = new();
            _modelsPathList.ForEach(p =>
            {
                modelNames.Add(p.Split(new char[] {@"\"[0], "/"[0]}).Last());
            });
            tf.value = string.Join("; ", modelNames);
        }
    }
}
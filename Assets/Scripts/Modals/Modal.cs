using System;
using System.Collections;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class Modal : MonoBehaviour
{
    protected XRUIModal XRUIModal;
    protected UIDocument UIDocument;

    protected virtual IEnumerator Start()
    {
        XRUIModal = GetComponent<XRUIModal>();
        UIDocument = GetComponent<UIDocument>();
        yield return new WaitUntil(() => XRUIModal.isActiveAndEnabled);
    }
}

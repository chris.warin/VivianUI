using System.Collections.Generic;
using de.ugoe.cs.vivian.core;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.Networking;
using System.Runtime.CompilerServices;
using com.chwar.xrui;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class Utils : ScriptableObject
{
    /// <summary>
    /// Contains all Vivian Specs
    /// </summary>
    public enum ElementType
    {
        DISPLAY,
        LIGHT,
        TEXT,
        SOUND,
        MOVABLE,
        ROTATABLE,
        BUTTON,
        TOUCHSCREEN,
        SLIDER,
        NONE
    }
    
    /*CREATE
     -------------------------------------------------------------------------------------------------------------------*/
    
    public static void CreateState(StateSpec stateSpec)
    {
        VivianUI.States.Add(stateSpec);
    }
    
    /*READ
     -------------------------------------------------------------------------------------------------------------------*/
    
    /*Find One*/
    public static InteractionElementSpec GetInteractionElement<T>(string element)
    {
        return VivianUI.InteractionElements.Find(e => e.Name.Equals(element) && e.GetType() == typeof(T));
    }
    
    public static VisualizationElementSpec GetVisualizationElement<T>(string element)
    {
        return VivianUI.VisualizationElements.Find(e => e.Name.Equals(element) && e.GetType() == typeof(T));
    }
    
    public static StateSpec GetState(string stateName)
    {
        return VivianUI.States.Find(s => s.Name.Equals(stateName));
    }      
    
    public static StateSpec GetCurrentState()
    {
        // return VivianUI.States[VivianUI.Instance.CurrentSituationIndex];
        var selected = XRUI.Instance.xruiGridController.XruiRoot.Q(null, "xrui__list__item--selected").Q<TextField>(null, "unity-text-field");
        var situationName = selected.ClassListContains("xrui__list__item--renaming")
            ? (string) selected.userData
            : selected.value;
        return GetState(situationName);
    }    
    
    public static int GetStateIndex(string stateName)
    {
        return VivianUI.States.FindIndex(s => s.Name.Equals(stateName));
    }
    
    public static TransitionSpec GetTransition(string specName, EventSpec type)
    {
        return VivianUI.Transitions.Find(t => t.InteractionElement.Name.Equals(specName) 
                                              && t.Event.Equals(type));
    }
    
    public static TransitionSpec GetTransition(string sourceState, string specName, EventSpec type)
    {
        return VivianUI.Transitions.Find(t => t.SourceState.Name.Equals(sourceState) && t.InteractionElement.Name.Equals(specName) 
                                              && t.Event.Equals(type));
    }

    public static VisualizationArraySpec GetVisualizationArray(string array)
    {
        return VivianUI.VisualizationArrays.Find(a => a.Name.Equals(array));
    }
    
    /*Find All*/
    
    public static List<InteractionElementSpec> GetInteractionElements(string element)
    {
        return VivianUI.InteractionElements.FindAll(e => e.Name.Equals(element));
    }
    
    public static List<VisualizationElementSpec> GetVisualizationElements(string element)
    {
        return VivianUI.VisualizationElements.FindAll(e => e.Name.Equals(element));
    }
    
    public static List<StateSpec> GetStates(string stateName)
    {
        return VivianUI.States.FindAll(s => s.Name.Equals(stateName));
    }
    
    public static List<TransitionSpec> GetTransitions(string linkedSituation)
    {
        return VivianUI.Transitions.FindAll(t =>
            t.DestinationState.Name.Equals(linkedSituation) || t.SourceState.Name.Equals(linkedSituation));
    }
    
    public static List<TransitionSpec> GetTransitions(string source, string destination)
    {
        return VivianUI.Transitions.FindAll(t => t.SourceState.Name.Equals(source) && t.DestinationState.Name.Equals(destination));
    }
    
    public static List<VisualizationArraySpec> GetVisualizationArrays(string array)
    {
        return VivianUI.VisualizationArrays.FindAll(a => a.Name.Equals(array));
    }
    
    /*UPDATE
     -------------------------------------------------------------------------------------------------------------------*/

    public static void UpdateState(string stateName)
    {
        var stateIndex = VivianUI.States.FindIndex(s => s.Name.Equals(stateName));
        var state = VivianUI.States[stateIndex];
        VivianUI.States.RemoveAt(stateIndex);
        VivianUI.States.Insert(0, state);
    }

    public static void SetCurrentState(string stateName)
    {
        VivianUI.Instance.CurrentSituationIndex = GetStateIndex(stateName);
    }
    
    /*Delete*/
    
    public static void DeleteInteractionElement<T>(string element)
    {
        VivianUI.InteractionElements.Remove(GetInteractionElement<T>(element));
    }
    
    public static void DeleteVisualizationElement<T>(string element)
    {
        VivianUI.VisualizationElements.Remove(GetVisualizationElement<T>(element));
    }
    
    public static bool DeleteState(string stateName)
    {
        // Delete all associated transitions
        foreach (var transition in GetTransitions(stateName))
        {
            DeleteTransition(transition);
        }
        return VivianUI.States.Remove(GetState(stateName));
    }
    
    public static bool DeleteTransition(string specName, EventSpec type)
    {
        return VivianUI.Transitions.Remove(GetTransition(specName, type));
    }
    
    public static bool DeleteTransition(TransitionSpec transition)
    {
        return VivianUI.Transitions.Remove(transition);
    }
    
    public static bool DeleteVisualizationArray(string array)
    {
        return VivianUI.VisualizationArrays.Remove(GetVisualizationArray(array));
    }

    /*public [] GetStateConditionsForInteractionElement(string state, string interactionElement)
    {
        return GetState(state).Conditions.Where(s => s.))
    }*/
    
    /*public IVisualizationSpec[] GetStateConditionsForVisualizationElement(string state, string interactionElement)
    {
        return GetState(state).Conditions.Where(s => );
    }*/
    
    /*Enum <-> Spec Parser*/

    public static ElementType ParseVisualizationSpec(VisualizationElementSpec spec)
    {
        ElementType type = ElementType.NONE;
        switch (spec)
        {
            case ScreenSpec:
                type = ElementType.DISPLAY;
                break;
            case LightSpec:
                type = ElementType.LIGHT;
                break;
            // TODO Figure out what's going on with Text Specs
            /*case TextSpec:
                type = ElementType.TEXT;
                break;*/
            case SoundSourceSpec:
                type = ElementType.SOUND;
                break;
        }
        return type;
    }

    public static ElementType ParseInteractionSpec(InteractionElementSpec spec)
    {
        ElementType type = ElementType.NONE;
        switch (spec)
        {
            case MovableSpec:
                type = ElementType.MOVABLE;
                break;
            case RotatableSpec:
                type = ElementType.ROTATABLE;
                break;
            case ButtonSpec:
                type = ElementType.BUTTON;
                break;
            case TouchAreaSpec:
                type = ElementType.TOUCHSCREEN;
                break;
            case SliderSpec:
                type = ElementType.SLIDER;
                break;
        }
        return type;
    }
    
    public static bool IsPointerOverUI()
    {
        return VivianUI.Instance.XruiElements.Any(ui => ui.PointerOverUI);
    }
}

/// <summary>
/// https://gist.github.com/krzys-h/9062552e33dd7bd7fe4a6c12db109a1a#gistcomment-3866629
/// Async awaitable UnityWebRequest <br/><br/>
/// Usage example: <br/><br/>
/// UnityWebRequest www = new UnityWebRequest(); <br/>
/// // do unitywebrequest setup here here... <br/>
/// await www.SendWebRequest(); <br/>
/// Debug.Log(req.downloadHandler.text); <br/>
/// </summary>
public struct UnityWebRequestAwaiter : INotifyCompletion
{
    private UnityWebRequestAsyncOperation asyncOp;
    private Action continuation;

    public UnityWebRequestAwaiter(UnityWebRequestAsyncOperation asyncOp)
    {
        this.asyncOp = asyncOp;
        continuation = null;
    }

    public bool IsCompleted { get { return asyncOp.isDone; } }

    public void GetResult() { }

    public void OnCompleted(Action continuation)
    {
        this.continuation = continuation;
        asyncOp.completed += OnRequestCompleted;
    }

    private void OnRequestCompleted(AsyncOperation obj)
    {
        continuation?.Invoke();
    }
}

public static class ExtensionMethods
{
    public static UnityWebRequestAwaiter GetAwaiter(this UnityWebRequestAsyncOperation asyncOp)
    {
        return new UnityWebRequestAwaiter(asyncOp);
    }
}

[Serializable]
public struct ContextualMenuEntries
{
    public Texture2D icon;
    public string text;
    public UnityEvent<PointerDownEvent, object> actionOnClick;
}

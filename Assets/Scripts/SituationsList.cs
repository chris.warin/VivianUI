using System;
using System.Collections.Generic;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using de.ugoe.cs.vivian.core;
using UnityEngine;
using UnityEngine.UIElements;
using VivianComponents;
using Button = UnityEngine.UIElements.Button;

public class SituationsList : MonoBehaviour
{
    public List<ContextualMenuEntries> contextualMenuEntries;
    private XRUIList _situationsList;
    private Action _situationCreatedCallback;
    private bool _triggerCallbackOnce;
    private bool _triggerCallbackFlag;
    
    private void Start()
    {
        _situationsList = GetComponent<XRUIList>();
        // Put event handlers in start to avoid over subscription
        _situationsList.AddButton.clicked += () => AddSituationUI("New Situation", true, false, true);
    }

    private void OnEnable()
    {
        // Relink to XRUI component
        _situationsList = GetComponent<XRUIList>();
    }

    /// <summary>
    /// Adds a new situation to the situation list.
    /// </summary>
    /// <param name="situationName">Name of the situation.</param>
    /// <param name="bSelectSituation">True if the situation should be visually selected.</param>
    /// <param name="bSetDefault">True to set the situation as start situation.</param>
    /// <param name="createVivianState">True to create a Vivian state with the situation.</param>
    public void AddSituationUI(string situationName, bool bSelectSituation, bool bSetDefault = false, bool createVivianState = false)
    {
        VisualElement situation = _situationsList.AddElement(bSelectSituation, UpdateElementPicker);
        var textField = situation.Q<TextField>(null, "unity-text-field");
        
        // Make sure the default situation name isn't used in other situations to avoid duplicates
        var index = VivianUI.States.FindAll(s => s.Name.StartsWith(situationName)).Count;
        textField.value = index >= 0 && !createVivianState || index == 0 && createVivianState ? situationName : situationName + $" ({index})";
        textField.userData = textField.value;

        // Display the home icon if this state is the start situation
        if (bSetDefault)
            situation.ElementAt(0).AddToClassList("xrui__list__item--default");

        // Set up the text field
        if (createVivianState)
        {
            // A vivian state will be created once the text field loses focus
            textField.RegisterCallback<FocusOutEvent>(CreateVivianState);
            textField.isReadOnly = false;
            // TODO #11 Focusing here throws a bug related to UI Toolkit.
            textField.Focus();
            textField.AddToClassList("xrui__list__item--renaming");
        }

        // Set up a callback to update the state
        textField.RegisterCallback<FocusOutEvent>(_ =>
        {
            if (textField.ClassListContains("xrui__list__item--renaming"))
            {
                // Only accept update if state with given name doesn't exist or if fallback to previous name
                if (Utils.GetState(textField.value) != null  && !textField.value.Equals(textField.userData.ToString()))
                {
                    XRUI.Instance.ShowAlert(XRUI.AlertType.Danger, "Error", "A situation with this name already exists!");
                    // Fallback to the previous value
                    textField.value = (string) textField.userData;
                }
                else
                {
                    var state = Utils.GetState(textField.userData as string);
                    state.Name = textField.value;
                }
                textField.isReadOnly = true;
                textField.RemoveFromClassList("xrui__list__item--renaming");
            }
        });
        
        // Setup the contextual menu for this situation
        var contextualMenu = situation.Q<Button>("More");
        contextualMenu.clicked += () => ShowContextualMenu(contextualMenu, textField.value);
    }

    /// <summary>
    /// Updates all Vivian components in the element picker with the selected situation.
    /// </summary>
    /// <param name="obj"></param>
    private void UpdateElementPicker(PointerDownEvent obj = null)
    {
        // TODO Keep a list of VivianComponent updated and access it instead of doing a Find every time (resource-intensive call) 
        foreach (var component in FindObjectsOfType<VivianComponent>())
        {
            component.OnComponentUpdated();
        }
    }

    /// <summary>
    /// Registers the provided callback when a new situation is created.
    /// </summary>
    /// <param name="callback">Callback to register.</param>
    /// <param name="triggerOnce">True if the callback should only be fired once.</param>
    public void RegisterCallbackOnSituationCreation(Action callback, bool triggerOnce)
    {
        this._situationCreatedCallback = callback;
        this._triggerCallbackOnce = triggerOnce;
        this._triggerCallbackFlag = false;
    }

    /// <summary>
    /// Resets the situation list visually.
    /// </summary>
    public void RemoveAllSituationsUI()
    {
        _situationsList.RemoveAllElements();
    }

    /// <summary>
    /// Callback called when the text field of a situation loses the focus (i.e., the user finished the situation).
    /// Creates a Vivian State with the name.
    /// </summary>
    /// <param name="evt">The Focus Out Event.</param>
    private void CreateVivianState(FocusOutEvent evt)
    {
        var textField = ((TextField) evt.target);
        var state = new StateSpec(textField.value);
        if (Utils.GetState(textField.value) != null)
        {
            XRUI.Instance.ShowAlert(XRUI.AlertType.Danger, "Error", "A situation with this name already exists!");
            textField.value = (string) textField.userData;
            state.Name = textField.value;
        }
        textField.isReadOnly = true;
        textField.RemoveFromClassList("xrui__list__item--renaming");

        Utils.CreateState(state);
        if (_situationCreatedCallback is not null &&
            (!_triggerCallbackOnce || _triggerCallbackOnce && !_triggerCallbackFlag))
        {
            _situationCreatedCallback.Invoke();
            _triggerCallbackFlag = true;
        }
        // Trigger element picker update
        UpdateElementPicker();
        textField.UnregisterCallback<FocusOutEvent>(CreateVivianState);
    }

    /// <summary>
    /// Creates a contextual menu when the associated button is clicked.
    /// </summary>
    /// <param name="parentElement">The clicked element which is the origin for the positioning of the contextual menu.</param>
    /// <param name="situationName">Name of the situation on which the menu is open.</param>
    private void ShowContextualMenu(VisualElement parentElement, string situationName)
    {
        var contextualMenu = XRUI.Instance.ShowContextualMenu(null, new Vector2(parentElement.worldBound.x, parentElement.parent.worldBound.y),
            true, _situationsList.List.resolvedStyle.width, Single.NaN);
        contextualMenu.menuElementTemplate = Resources.Load<VisualTreeAsset>("DefaultContextualMenuElement");
        foreach (var entry in contextualMenuEntries)
        {
            var el = contextualMenu.AddMenuElement();
            el.Q("Icon").style.backgroundImage = entry.icon;
            el.Q<Label>("Text").text = entry.text;
            el.RegisterCallback<PointerDownEvent, string>(entry.actionOnClick.Invoke, situationName);
        }
    }
    
    /*Animation
    -------------------------------------------------------------------------------------------------------------------*/

    /// <summary>
    /// Convenience method to hide the list in AR mode when a touch is made outside the list. 
    /// </summary>
    /// <param name="evt"></param>
    private void CheckForClicks(PointerDownEvent evt)
    {
        if (!Utils.IsPointerOverUI())
            Animate(false);
    }

    /// <summary>
    /// Animation method to show/hide the list in AR mode.
    /// </summary>
    /// <param name="bShow"></param>
    public void Animate(bool bShow)
    {
        if(bShow)
            _situationsList.List.parent.RegisterCallback<PointerDownEvent>(CheckForClicks);
        else
            _situationsList.List.parent.UnregisterCallback<PointerDownEvent>(CheckForClicks);

        // Workaround to detect clicks outside of the list. This requires the list container's layer to be under the card's
        _situationsList.List.parent.pickingMode = PickingMode.Position;
        _situationsList.List.ToggleInClassList("animate");
    }
    
    /// <summary>
    /// Animation method to show/hide the list in AR mode.
    /// </summary>
    public void Animate()
    {
        Animate(_situationsList.List.ClassListContains("animate"));
    }
    
    /*Contextual menu actions
     -------------------------------------------------------------------------------------------------------------------*/

    /// <summary>
    /// Opens the time-based transition creation modal.
    /// </summary>
    /// <param name="evt">Contextual Menu pointer event (nullable)</param>
    /// <param name="situationName"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void AddTimeBasedTransition(PointerDownEvent evt, object situationName)
    {
        // TODO #15
        throw new NotImplementedException();
    }
    
    /// <summary>
    /// Makes the selected situation as default.
    /// </summary>
    /// <param name="evt">Contextual Menu pointer event (nullable)</param>
    /// <param name="situationName"></param>
    public void MakeDefaultSituation(PointerDownEvent evt, object situationName)
    {
        var previousDefault = ((VisualElement) evt.target).panel.visualTree.Q(null, "xrui__list__item--default");
        var newDefault = ((VisualElement) evt.target).panel.visualTree.Query(null, "xrui__list__item")
            .Where(s => s.Q<TextField>(null, "unity-text-field").text.Equals(situationName)).First();
        
        // Adapt UI
        previousDefault.ToggleInClassList("xrui__list__item--default");
        newDefault.ToggleInClassList("xrui__list__item--default");
        // Put the situation at the top of the list (UI)
        newDefault.parent.SendToBack();
        
        // Adapt default state in state machine 
        Utils.UpdateState(situationName as string);
    }
    
    /// <summary>
    /// Makes the current situation renamable.
    /// </summary>
    /// <param name="evt">Contextual Menu pointer event (nullable)</param>
    /// <param name="situationName"></param>
    public void RenameSituation(PointerDownEvent evt, object situationName)
    {
        var textField = ((VisualElement) evt.target).panel.visualTree.Q(null, "xrui__list__item--selected")
            .Q<TextField>(null, "unity-text-field");
        textField.Focus();
        textField.userData = situationName;
        textField.AddToClassList("xrui__list__item--renaming");
        textField.isReadOnly = false;
    }
    
    /// <summary>
    /// Deletes the selected situation.
    /// </summary>
    /// <param name="evt">Contextual Menu pointer event (nullable)</param>
    /// <param name="situationName"></param>
    public void DeleteSituation(PointerDownEvent evt, object situationName)
    {
        // TODO #13 Add a confirmation modal

        if (_situationsList.GetListCount() == 1)
        {
            XRUI.Instance.ShowAlert(XRUI.AlertType.Warning, "Cannot delete situation", "There is only one situation left.");
            return;
        }
        
        if (Utils.DeleteState(situationName as string))
        {
            // Delete UI
            var situation = ((VisualElement) evt.target).panel.visualTree.Q(null, "xrui__list__item--selected");
            bool isSituationDefault = situation.ClassListContains("xrui__list__item--default");
            situation.RemoveFromHierarchy();
            // Select the first situation in the list
            var situationToSelect = _situationsList.List.Query(null, "xrui__list__item").First();
            // If we deleted the current default situation, the next one in the list is set as default
            if (isSituationDefault)
            {
                situationToSelect.ToggleInClassList("xrui__list__item--default");
                // Adapt default state in state machine 
                Utils.UpdateState(situationToSelect.Q<TextField>(null, "unity-text-field").text);
            }

            _situationsList.SelectElement(situationToSelect);
        }
    }
}

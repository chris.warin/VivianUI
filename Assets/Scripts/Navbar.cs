using System;
using com.chwar.xrui;
using UnityEngine;
using UnityEngine.UIElements;

public class Navbar : MonoBehaviour
{
    private Button _switchButton;
    private Button _situationsButton;
    private VisualElement _navbar;
    private Texture2D _playTex;
    private Texture2D _editTex;
    private SituationsList _situationList;
    
    void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;
        root.Q<Button>("Open").clicked += OpenMenu;
        root.Q<Button>("Save").clicked += SaveProject;
        root.Q<Button>("Export").clicked += ExportProject;
        _navbar = root.Q("Navbar");
        _switchButton = root.Q<Button>("SwitchMode");
        _switchButton.clicked += () => SwitchMode(!VivianUI.Instance.IsPlaying);
        _situationsButton = root.Q<Button>("ShowSituations");
        _situationsButton.clicked += OpenSituations;
        _situationsButton.visible = XRUI.IsCurrentReality(XRUI.RealityType.AR);

        _playTex = Resources.Load<Texture2D>("Icons/play_circle");
        _editTex = Resources.Load<Texture2D>("Icons/edit");
        _switchButton.style.backgroundImage = _playTex;

        _situationList = FindObjectOfType<SituationsList>();
    }

    /// <summary>
    /// Button to show the situation list in AR mode.
    /// </summary>
    private void OpenSituations()
    {
        _situationList.Animate();
    }

    /// <summary>
    /// Button to switch between edit mode and play mode.
    /// </summary>
    /// <param name="bPlay"></param>
    public void SwitchMode(bool bPlay)
    {
        _switchButton.style.backgroundImage = bPlay ? _editTex : _playTex;
        _navbar.EnableInClassList("play-mode", bPlay);
        VivianUI.Instance.SwitchMode(bPlay);
    }

    /// <summary>
    /// Exports the current project.
    /// </summary>
    /// <exception cref="NotImplementedException"></exception>
    private void ExportProject()
    {
        // TODO #5
        throw new NotImplementedException();
    }

    /// <summary>
    /// Saves the current project.
    /// </summary>
    private void SaveProject()
    {
        VivianUI.Instance.ProjectFilesController.SaveProject();
        XRUI.Instance.ShowAlert(XRUI.AlertType.Success, "Project saved successfully.");
    }

    /// <summary>
    /// Opens the main menu.
    /// </summary>
    private void OpenMenu()
    {
        VivianUI.Instance.Menu.Animate(true);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using de.ugoe.cs.vivian.core;
using VivianComponents;
using static Utils;
using Button = UnityEngine.UIElements.Button;

public class ElementPicker : MonoBehaviour
{
    public string mainText;
    public List<InspectorSpecsUI> listSpecs;
    public Dictionary<string, List<InspectorSpecsUI>> SpecsUI;

    // Manipulator script
    private ModelManipulator _manipulator;
    // Root visual element of the card
    private VisualElement _root;
    // Main UI of the card, used to add Vivian elements to selected object
    private VisualElement _elementPicker;
    // Main container of the Card where UI Elements are contained
    private ScrollView _container;
    // Button to add elements
    private Button _addElementButton;
    // XRUI Reference to the Card
    private XRUICard _card;
    // Textures for the "Add Element" button
    private Texture2D _addTex;
    private Texture2D _arrowUpTex;
    
    /// <summary>
    /// Updates the card accordingly to the selected object.
    /// </summary>
    /// <param name="element">Selected object</param>
    public void UpdateElementPicker(string element)
    {
        // Delete all elements
        _root.Query("Element").ForEach(ve => DeleteElementUI(ve.parent));
        
        InitSpecsUI(_manipulator.SelectedElement.name);

        // Create UI elements for each attached spec
        foreach (var spec in GetInteractionElements(element))
        {
            AddElementUI(ParseInteractionSpec(spec));
        }
        foreach (var spec in GetVisualizationElements(element))
        {
            AddElementUI(ParseVisualizationSpec(spec));
        }
    }

    private void InitSpecsUI(string elementName)
    {
        if (!SpecsUI.ContainsKey(elementName))
        {
            SpecsUI.Add(elementName, listSpecs);
        }
    }
    
    void OnEnable()
    {
        _manipulator = VivianUI.Instance.ModelManipulator;
        _root = GetComponent<UIDocument>().rootVisualElement;
        _card = GetComponent<XRUICard>();
        
        // Instantiate Element Picker
        _elementPicker = XRUI.Instance.GetUIElement("ElementPicker").Instantiate().ElementAt(0);
        // Set height transition
        _elementPicker.style.transitionProperty = new List<StylePropertyName> {new("height")};
        _elementPicker.style.transitionDuration = new List<TimeValue> {new(500, TimeUnit.Millisecond)};
        
        // Query Element Picker buttons from UXML and attach event handlers
        _elementPicker.Q<Button>("Display").clicked += () => AddElement(ElementType.DISPLAY);
        _elementPicker.Q<Button>("Light").clicked += () => AddElement(ElementType.LIGHT);
        _elementPicker.Q<Button>("Text").clicked += () => AddElement(ElementType.TEXT);
        _elementPicker.Q<Button>("Sound").clicked += () => AddElement(ElementType.SOUND);
        _elementPicker.Q<Button>("Movable").clicked += () => AddElement(ElementType.MOVABLE);
        _elementPicker.Q<Button>("Rotatable").clicked += () => AddElement(ElementType.ROTATABLE);
        _elementPicker.Q<Button>("Button").clicked += () => AddElement(ElementType.BUTTON);
        _elementPicker.Q<Button>("Touchscreen").clicked += () => AddElement(ElementType.TOUCHSCREEN);
        _elementPicker.Q<Button>("Slider").clicked += () => AddElement(ElementType.SLIDER);
        _elementPicker.Q<Label>("MainTitle").text = mainText;
        _container = _root.Q<ScrollView>("MainContainer");
        _addElementButton = _root.Q<Button>("AddElement");
        _addElementButton.style.display = DisplayStyle.None;
        _addElementButton.clicked += ShowElementPicker;
        _card.SetCloseButtonAction(() =>_manipulator.Unselect());
        
        // Add Element Picker to the card
        _card.AddUIElement(_elementPicker, "MainContainer");

        _addTex = Resources.Load<Texture2D>("Icons/add");
        _arrowUpTex = Resources.Load<Texture2D>("Icons/arrow_up");
    }

    /// <summary>
    /// Called when clicking on the "Add Element" button.
    /// Switches the visibility of the Element Picker and switches the icon of the "Add Element" button.
    /// </summary>
    private void ShowElementPicker()
    {
        bool bShow = _elementPicker.style.height.Equals(0) || _elementPicker.style.display.Equals(DisplayStyle.None);
        _elementPicker.style.display = DisplayStyle.Flex;
        _elementPicker.style.height = bShow ? 300 : 0;
        _addElementButton.style.backgroundImage = _addElementButton.style.backgroundImage.Equals(_addTex) ? _arrowUpTex : _addTex;
        _container.Sort(SortElementPicker);
        
        // TODO #14 Fix Scroll behaviour for touch and mouse wheel
        //_container.ScrollTo(_elementPicker);
    }

    /// <summary>
    /// Updates the Vivian logic by adding an element to the JSON specification.
    /// </summary>
    /// <param name="type"></param>
    private void AddElement(ElementType type)
    {
        InteractionElementSpec interactionElementSpec = null;
        VisualizationElementSpec visualizationElementSpec = null;
        
        UpdateVivianElementTemplate(type);

        switch (type)
        {
            case ElementType.DISPLAY:
                //visualizationElementSpec = new ScreenSpec();
                break;
            case ElementType.LIGHT:
                visualizationElementSpec = new LightSpec(_manipulator.SelectedElement.name, Color.green);
                break;
            case ElementType.TEXT:
                //visualizationElementSpec = new TextSpec();
                break;
            case ElementType.SOUND:
                //visualizationElementSpec = new SoundSourceSpec();
                break;
            case ElementType.MOVABLE:
                //interactionElementSpec = new MovableSpec();
                break;
            case ElementType.ROTATABLE:
                //interactionElementSpec = new RotatableSpec();
                break;
            case ElementType.BUTTON:
                interactionElementSpec = new ButtonSpec(_manipulator.SelectedElement.name);
                break;
            case ElementType.TOUCHSCREEN:
                //interactionElementSpec = new TouchAreaSpec();
                break;
            case ElementType.SLIDER:
                //interactionElementSpec = new SliderSpec();
                break;
        }

        if (interactionElementSpec != null)
        {
            VivianUI.InteractionElements.Add(interactionElementSpec);
        }
        
        else if (visualizationElementSpec != null)
        {
            VivianUI.VisualizationElements.Add(visualizationElementSpec);
        }

        // Create corresponding UI
        AddElementUI(type);
        // Add the element to the list of specs of selected object
        // _elementsSpecs[_manipulator.SelectedElement.name].Add(type);
    }

    /// <summary>
    /// Updates Vivian logic by deleting an element from the JSON specification.
    /// </summary>
    /// <param name="elementName">Name of the element to delete</param>
    /// <param name="visualElement">Visual Element UI to delete</param>
    private void DeleteElement(string elementName, VisualElement visualElement)
    {
        Enum.TryParse<ElementType>(elementName.ToUpper(), out var type);
        switch (type)
        {
            case ElementType.DISPLAY:
                DeleteVisualizationElement<ScreenSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.LIGHT:
                DeleteVisualizationElement<LightSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.TEXT:
                DeleteVisualizationElement<TextSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.SOUND:
                DeleteVisualizationElement<SoundSourceSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.MOVABLE:
                DeleteInteractionElement<MovableSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.ROTATABLE:
                DeleteInteractionElement<RotatableSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.BUTTON:
                DeleteInteractionElement<ButtonSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.TOUCHSCREEN:
                DeleteInteractionElement<TouchAreaSpec>(_manipulator.SelectedElement.name);
                break;
            case ElementType.SLIDER:
                DeleteInteractionElement<SliderSpec>(_manipulator.SelectedElement.name);
                break;
        }
        DeleteElementUI(visualElement);
        
        // Delete associated script
        var element = SpecsUI[_manipulator.SelectedElement.name].Find(s => s.elementType.Equals(type)).currentUI;
        var elementPrefix = element.Substring(0, element.IndexOf('_'));
        Type elementScriptType = Type.GetType($"VivianComponents.{elementPrefix}");
        Destroy(GetComponent(elementScriptType));
    }
    
    /// <summary>
    /// Creates an UI Element for the newly created Vivian element.
    /// </summary>
    /// <param name="type">Element Type</param>
    private void AddElementUI(ElementType type)
    {
        // Add the element template
        string elementName = type.ToString().First() + type.ToString().Substring(1).ToLower();
        VisualElement visualElement = XRUI.Instance.GetUIElement("Element").Instantiate();
        visualElement.Q<Label>("SeparatorLabel").text = elementName;
        visualElement.Q<Button>("DeleteElement").clicked += () => DeleteElement(elementName, visualElement);
        _card.AddUIElement(visualElement, "MainContainer");

        // Get the correct Vivian component UI and associated script
        var elementUI = SpecsUI[_manipulator.SelectedElement.name].Find(s => s.elementType.Equals(type)).currentUI;
        var elementPrefix = elementUI.Substring(0, elementUI.IndexOf('_'));
        Type elementScriptType = Type.GetType($"VivianComponents.{elementPrefix}");
        if (elementScriptType is null)
        {
            throw new NullReferenceException(
                $"There is no associated script to the {elementUI} template. Create a C# script with the same prefix ({elementPrefix})");
        }
        visualElement.Q("MainContainer").Add(Resources.Load<VisualTreeAsset>($"Custom UI Templates/Vivian Components/{elementUI}").Instantiate());
        var elementScript = gameObject.GetComponent(elementScriptType) ?? gameObject.AddComponent(elementScriptType);
        ((VivianComponent) elementScript).ElementType = type;
        ((VivianComponent) elementScript).VisualElement = visualElement;
        
        // Disable the clicked button
        _elementPicker.Q<Button>(elementName).SetEnabled(false);
        _root.Q("MainContainer").Sort(SortElementPicker);

        // Hide Element picker, then Sort UI Elements to put the Element Picker at the bottom
        _elementPicker.style.display = DisplayStyle.None;
        _elementPicker.style.height = 0;
        _addElementButton.style.display = DisplayStyle.Flex;
        _addElementButton.style.backgroundImage = _addTex;
    }
    
    /// <summary>
    /// Removes the UI Element corresponding to the deleted spec.
    /// </summary>
    /// <param name="visualElement">Element to delete</param>
    private void DeleteElementUI(VisualElement visualElement)
    {
        visualElement.RemoveFromHierarchy();
        if (GetInteractionElements(_manipulator.SelectedElement.name).Count == 0 && GetVisualizationElements(_manipulator.SelectedElement.name).Count == 0)
        {
            _elementPicker.style.transitionDuration = new List<TimeValue> {new(0, TimeUnit.Millisecond)};
            _elementPicker.style.height = 300;
            _elementPicker.style.transitionDuration = new List<TimeValue> {new(500, TimeUnit.Millisecond)};
            _elementPicker.style.display = DisplayStyle.Flex;
            _addElementButton.style.display = DisplayStyle.None;
            _addElementButton.style.backgroundImage = _addTex;
        }
        // Enable associated button
        _elementPicker.Q<Button>(visualElement.Q<Label>("SeparatorLabel").text).SetEnabled(true);
    }

    /// <summary>
    /// Updates the current Vivian element UI template.
    /// </summary>
    /// <param name="type">Vivian element type.</param>
    /// <param name="templateName">Name of the template to use from the inspector UI list.</param>
    public void UpdateVivianElementTemplate(ElementType type, string templateName = null)
    {
        var current = SpecsUI[_manipulator.SelectedElement.name].Find(s => s.elementType.Equals(type));
        var currentIdx = SpecsUI[_manipulator.SelectedElement.name].FindIndex(s => s.elementType.Equals(type));
        var element = templateName is null ? current.elementUIList[0] : current.elementUIList.Find(e => e.Equals(templateName));
        current.currentUI = element;
        SpecsUI[_manipulator.SelectedElement.name][currentIdx] = current;
    }

    /// <summary>
    /// Updates the Vivian UI element with the current template.
    /// </summary>
    /// <param name="visualElement">Container element to fill.</param>
    /// <param name="type">Vivian element type.</param>
    public void ChangeVivianElement(VisualElement visualElement, ElementType type)
    {
        // Remove current "page"
        var container = visualElement.Q("MainContainer");
        container.RemoveAt(0);
        var element = SpecsUI[_manipulator.SelectedElement.name].Find(s => s.elementType.Equals(type)).currentUI;
        var elementUI = Resources.Load<VisualTreeAsset>($"Custom UI Templates/Vivian Components/{element}").Instantiate();
        container.Add(elementUI);
    }

    /// <summary>
    /// Removes all the Vivian Components scripts that are attached to the element picker.
    /// </summary>
    public void RemoveAllVivianComponents()
    {
        foreach (var comp in GetComponents<VivianComponent>())
        {
            Destroy(comp);
        }
    }
    
    /// <summary>
    /// Sorts the list of Elements in the UIDocument by visibility.
    /// Effectively puts the element picker at the end of the main container.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private static int SortElementPicker(VisualElement x, VisualElement y)
    {
        x = x.GetType() == typeof(TemplateContainer) ? x.ElementAt(0) : x;
        y = y.GetType() == typeof(TemplateContainer) ? y.ElementAt(0) : y;
        return x.style.height.Equals(300) ? 1 
            : y.style.height.Equals(300) ? -1 
            : 0;
    }
}

[Serializable]
public struct InspectorSpecsUI
{
    public ElementType elementType;
    public List<string> elementUIList;
    [HideInInspector]
    public string currentUI;
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class Menu : MonoBehaviour
{
    private XRUIMenu _menu;
    public List<ContextualMenuEntries> contextualProjectMenuEntries;
    public List<ContextualMenuEntries> contextualMainMenuEntries;

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => VivianUI.Instance.isActiveAndEnabled);
        
        _menu = GetComponent<XRUIMenu>();
        // Generate menu items by loading recent projects
        var projects = VivianUI.Instance.ProjectFilesController.GetRecentProjects();
        foreach (var project in projects)
        {
            AddProjectToMenu(project);
        }

        _menu.Menu.parent.AddToClassList("xrui__background--dark");
        _menu.Menu.parent.pickingMode = PickingMode.Position;
        _menu.CloseButton.visible = false;
        
        // Set side transition
        _menu.Menu.style.transitionProperty = new List<StylePropertyName> {new("left")};
        _menu.Menu.style.transitionDuration = new List<TimeValue> {new(300, TimeUnit.Millisecond)};

        // Add event handler on main button
        _menu.MainButton.clicked += () => ShowMainContextualMenu(_menu.MainButton);
    }

    /// <summary>
    /// Redraws the menu entries.
    /// </summary>
    public void UpdateEntries()
    {
        // Redraw menu with potential new entries
        _menu.RemoveAllElements();
        AddProjectToMenu(VivianUI.Instance.ProjectFilesController.GetRecentProjects().Last());
    }

    private void AddProjectToMenu(ProjectJsonWrapper project)
    {
        var element = _menu.AddElement();
        element.Q<Label>("ProjectName").text = project.projectName;
        element.Q<Label>("LastAccess").text = project.lastEdit;
        Texture2D tex = new Texture2D(64, 64);
        tex.LoadImage(project.base64Screenshot);
        element.Q("Thumbnail").style.backgroundImage = tex;

        element.Q<Button>("Open").clicked += () => LoadProject(project, false);
        var more = element.Q<Button>("More");
        more.clicked += () =>
        {
            ShowEntryContextualMenu(more, project);
        };
    }

    private void ShowMainContextualMenu(VisualElement parentElement)
    {
        var contextualMenu = XRUI.Instance.ShowContextualMenu(null, new Vector2(parentElement.worldBound.x, parentElement.worldBound.y),
            false, _menu.Menu.resolvedStyle.width, _menu.MainButton.resolvedStyle.width);
        contextualMenu.menuElementTemplate = Resources.Load<VisualTreeAsset>("DefaultContextualMenuElement");
        
        foreach (var entry in contextualMainMenuEntries)
        {
            var el = contextualMenu.AddMenuElement();
            el.Q<Label>("Text").text = entry.text; 
            el.RegisterCallback<PointerDownEvent, object>(entry.actionOnClick.Invoke, null);
        }
    }
    
    /// <summary>
    /// Creates a contextual menu when the associated button is clicked.
    /// </summary>
    /// <param name="parentElement">The clicked element which is the origin for the positioning of the contextual menu.</param>
    /// <param name="situationName">Name of the situation on which the menu is open.</param>
    private void ShowEntryContextualMenu(VisualElement parentElement, ProjectJsonWrapper project)
    {
        var contextualMenu = XRUI.Instance.ShowContextualMenu(null, new Vector2(parentElement.worldBound.x, parentElement.worldBound.y),
            true, _menu.Menu.resolvedStyle.width, Single.NaN);
        contextualMenu.menuElementTemplate = Resources.Load<VisualTreeAsset>("DefaultContextualMenuElement");
        
        // Add class to identify currently selected visual element
        parentElement.panel.visualTree.Q(null, "xrui__menu__item--selected")?.ToggleInClassList("xrui__menu__item--selected");
        parentElement.parent.parent.parent.AddToClassList("xrui__menu__item--selected");
        
        foreach (var entry in contextualProjectMenuEntries)
        {
            var el = contextualMenu.AddMenuElement();
            el.Q("Icon").style.backgroundImage = entry.icon;
            el.Q<Label>("Text").text = entry.text; 
            el.RegisterCallback<PointerDownEvent, ProjectJsonWrapper>(entry.actionOnClick.Invoke, project);
        }
    }

    /// <summary>
    /// Loads a project.
    /// </summary>
    /// <param name="project">Project to load.</param>
    /// <param name="bPlay">Run the project in Vivian mode directly.</param>
    private void LoadProject(ProjectJsonWrapper project, bool bPlay)
    {
        VivianUI.Instance.LoadProject(project);
        // Call from Navbar to update UI directly
        FindObjectOfType<Navbar>().SwitchMode(bPlay);
        Animate(false);
        AllowCollapsing(true);
        XRUI.Instance.ShowAlert(XRUI.AlertType.Success, "Project loaded successfully.");
    }
    
    /*Menu animation
    -------------------------------------------------------------------------------------------------------------------*/

    /// <summary>
    /// Animates the menu in and out of view.
    /// </summary>
    /// <param name="bShow"></param>
    public void Animate(bool bShow)
    {
        _menu.Menu.parent.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
        _menu.Menu.style.left = new StyleLength(bShow ? 0 : -_menu.Menu.contentRect.width - 50);
        _menu.Menu.parent.EnableInClassList("xrui__background--dark", bShow);
        
        if(!bShow)
            StartCoroutine(HideContainer());
    }
    
    private IEnumerator HideContainer()
    {
        yield return new WaitForSeconds(0.5f);
        _menu.Menu.parent.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
    }

    /// <summary>
    /// Determines whether the menu is allowed to collapse out of view when clicked outside.
    /// </summary>
    /// <param name="b">Whether to allow collapsing.</param>
    public void AllowCollapsing(bool b)
    {
        if (b)
        {
            _menu.CloseButton.visible = true;
            _menu.CloseButton.clicked += () => Animate(false);
            _menu.Menu.parent.RegisterCallback<PointerDownEvent>(CheckForClicks);
        }
        else
            _menu.Menu.parent.UnregisterCallback<PointerDownEvent>(CheckForClicks);
    }
    
    /// <summary>
    /// Convenience method to collapse the menu when unfocused.
    /// </summary>
    /// <param name="evt"></param>
    private void CheckForClicks(PointerDownEvent evt)
    {
        if(!_menu.PointerOverUI)
            Animate(false);
    }
    
    /*Contextual project menu methods
     -------------------------------------------------------------------------------------------------------------------*/

    /// <summary>
    /// Opens a project in "play" mode.
    /// </summary>
    /// <param name="evt">Pointer event (can be null).</param>
    /// <param name="project">The project JSON object.</param>
    public void RunProject(PointerDownEvent evt, object project)
    {
        LoadProject(project as ProjectJsonWrapper, true);        
    }

    /// <summary>
    /// Removes a project from the project list.
    /// </summary>
    /// <param name="evt">Pointer event (can be null).</param>
    /// <param name="project">The project JSON object.</param>
    public void RemoveProject(PointerDownEvent evt, object project)
    {
        List<ProjectJsonWrapper> projects = VivianUI.Instance.ProjectFilesController.GetRecentProjects();
        var projectIdx = projects.FindIndex(p => p.projectName.Equals(((ProjectJsonWrapper) project).projectName));
        projects.RemoveAt(projectIdx);
        VivianUI.Instance.ProjectFilesController.SetRecentProjects(projects);
        
        // Remove UI
        var projectUI = ((VisualElement) evt.target).panel.visualTree.Q(null, "xrui__menu__item--selected");
        projectUI.RemoveFromHierarchy();
    }
    
    /*Contextual main menu methods
    -------------------------------------------------------------------------------------------------------------------*/

    /// <summary>
    /// Opens the project creation modal. 
    /// </summary>
    /// <param name="evt">Pointer event (can be null).</param>
    /// <param name="o">The contextual menu object (can be null).</param>
    public void CreateNewProject(PointerDownEvent evt, object o)
    {
        VivianUI.Instance.ShowModal("CreateProjectModal");
    }
    
    /// <summary>
    /// Opens the project import modal.
    /// </summary>
    /// <param name="evt">Pointer event (can be null).</param>
    /// <param name="o">The contextual menu object (can be null).</param>
    public void ImportProject(PointerDownEvent evt, object o)
    {
        VivianUI.Instance.ShowModal("ImportProjectModal");
    }
}

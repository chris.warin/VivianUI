using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using cakeslice;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using de.ugoe.cs.vivian.core;
using UnityEngine;

[RequireComponent(typeof(ProjectFilesController))]
[RequireComponent(typeof(ModelManipulator))]
public class VivianUI : MonoBehaviour
{
    // Singleton
    public static VivianUI Instance;

    // Manipulator
    public ModelManipulator ModelManipulator { get; private set; }
    // Situations List
    public SituationsList SituationsList { get; private set; }
    // Main Menu
    public Menu Menu { get; private set; }
    // Element Picker
    public ElementPicker ElementPicker { get; private set; }
    // Files Controller
    public ProjectFilesController ProjectFilesController { get; private set; }
    // List of XRUI Elements
    public List<XRUIElement> XruiElements { get; private set; }
    // Indicates if the application is in playing mode or edit mode
    public bool IsPlaying { get; private set; }

    // Functional Specification elements
    public static List<InteractionElementSpec> InteractionElements { get; private set; }
    public static List<VisualizationElementSpec> VisualizationElements { get; private set; }
    public static List<StateSpec> States { get; private set; }
    public static List<TransitionSpec> Transitions { get; private set; }
    public static List<VisualizationArraySpec> VisualizationArrays { get; private set; }

    public int CurrentSituationIndex;
    
    // Vivian Framework prefab
    [SerializeField]
    private GameObject vivianPrefab;

    // Internal variables
    private GameObject _vivianInteraction;
    private GameObject _vivianInstance;
    private GameObject _model;
    private GameObject _xrui;
    private GameObject _modelBackup;

    /* Unity methods
     ---------------------------------------------------------------------------------*/

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    private void Start()
    {
        ModelManipulator = GetComponent<ModelManipulator>();
        ProjectFilesController = GetComponent<ProjectFilesController>();
        SituationsList = FindObjectOfType<SituationsList>();
        ElementPicker = FindObjectOfType<ElementPicker>();
        Menu = FindObjectOfType<Menu>();
        XruiElements = FindObjectsOfType<XRUIElement>().ToList();
        XruiElements.RemoveAll(e => e.GetType() == typeof(XRUIAlert));

        // Hide main UI
        _xrui = FindObjectOfType<XRUIGridController>().gameObject;
        _xrui.SetActive(false);

        // Get and Deactivate Vivian interactions
        GetVivianInteraction();
        foreach (Transform interaction in
           GameObject.Find("Interactions").transform.GetComponentsInChildren<Transform>())
        {
           if (interaction.name == "Interactions") continue;
           interaction.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Initializes Vivian arrays.
    /// </summary>
    internal static void InitArrays()
    {
        InteractionElements = new();
        VisualizationElements = new();
        States = new();
        Transitions = new();
        VisualizationArrays = new();
    }


    /* Public methods
    ---------------------------------------------------------------------------------*/
    
    /// <summary>
    /// Creates a VivianUI project.
    /// </summary>
    /// <param name="projectName">Name of the project to create.</param>
    /// <param name="projectPath">Path of the project.</param>
    /// <param name="modelPaths">Path of the model(s) to import.</param>
    /// <param name="createSubdir">Whether to create a subdirectory in the at the given path.</param>
    public async void CreateProject(string projectName, string projectPath, List<string> modelPaths, bool createSubdir) {
        _xrui.SetActive(true);
        InitArrays();
        
        // If creating a project from another running project, keep the state of the model
        bool modelActive = true;
        if (_model is not null)
        {
            modelActive = _model.activeSelf;
            Destroy(_model);
        }
        var models = await ProjectFilesController.CreateFiles(projectName, projectPath, modelPaths, createSubdir);
        _model = models[0];
        _model.SetActive(modelActive);
        
        // Create default state
        ElementPicker.RemoveAllVivianComponents();
        ElementPicker.SpecsUI = new Dictionary<string, List<InspectorSpecsUI>>();
        SituationsList.RemoveAllSituationsUI();
        SituationsList.AddSituationUI("Start", true, true);
        Utils.CreateState(new StateSpec("Start"));
        
        // Save project
        ProjectFilesController.SaveProject();
        
        // Update menu
        Menu.UpdateEntries();

        // Set the loaded model as target for the model manipulator and add colliders for triggering the element picker 
        ModelManipulator.SetTarget(_model.transform.GetChild(0).GetChild(0).gameObject);
        foreach (Transform child in _model.transform.GetChild(0).GetChild(0).GetComponentsInChildren<Transform>()) {
            child.gameObject.AddComponent<MeshCollider>();
        }
    }
    
    /// <summary>
    /// Loads a VivianUI project.
    /// </summary>
    /// <param name="project">The project JSON object.</param>
    public void LoadProject(ProjectJsonWrapper project) {
        var loadedModels = ProjectFilesController.LoadFiles(project.projectName, project.projectPath,
            project.modelPaths, project.projectSubdir);
        // If loading a project from another running project, keep the state of the model
        bool modelActive = true;
        if (_model is not null)
        {
            modelActive = _model.activeSelf;
            Destroy(_model);
        }
        _model = loadedModels[0];
        _model.SetActive(modelActive);
        _xrui.SetActive(true);

        // Create states
        ElementPicker.RemoveAllVivianComponents();
        ElementPicker.SpecsUI = project.specsUI;
        SituationsList.RemoveAllSituationsUI();
        foreach (var stateSpec in States)
        {
            SituationsList.AddSituationUI(stateSpec.Name, stateSpec.Equals(States[0]), stateSpec.Equals(States[0]));
        }

        // Set the loaded model as target for the model manipulator and add colliders for triggering the element picker 
        ModelManipulator.SetTarget(_model.transform.GetChild(0).GetChild(0).gameObject);
        foreach (Transform child in _model.transform.GetChild(0).GetChild(0).GetComponentsInChildren<Transform>()) {
            child.gameObject.AddComponent<MeshCollider>();
        }
    }
    
    /// <summary>
    /// Gets a modal from the XRUI Framework and displays it.
    /// </summary>
    /// <param name="modalName">Name of the modal template to use.</param>
    /// <exception cref="ArgumentException">Thrown if no matching script is found.</exception>
    public void ShowModal(string modalName)
    {
        Type t = Type.GetType($"Modals.{modalName}");
        if (t is null)
        {
            throw new ArgumentException($"No modal script matching the template '{modalName}' could be found. " +
                                        $"Make sure it uses the namespace \"Modals\".");
        }
        XRUI.Instance.CreateModal(modalName, t);
    }

    /// <summary>
    /// Switches the project from edit mode to play mode.
    /// </summary>
    /// <param name="bPlay">Whether to start the "play" mode.</param>
    public void SwitchMode(bool bPlay)
    {
        // Only switch manipulation mode if there is a difference between current and targeted mode
        // E.g. if we're in play mode on a project and loading another project in play mode, don't switch
        if(IsPlaying != bPlay)
            StartCoroutine(SwitchManipulation());
        if (bPlay)
        {
            ProjectFilesController.SaveVivianSpecificationToDisk();
            StartVivianFramework("file://" + ProjectFilesController.InternalProjectPath, _model);
        }
        // Stop the Framework 
        if (IsPlaying)
        {
            _model = _modelBackup;
            _model.SetActive(true);
            ModelManipulator.SetTarget(_model);
            Destroy(_vivianInstance);
        }

        IsPlaying = bPlay;
    }
    
    /* Private methods
    ---------------------------------------------------------------------------------*/

    /// <summary>
    /// Starts the Vivian Framework by reading the functional specification.
    /// </summary>
    /// <param name="url">The (local) URL / model path.</param>
    /// <param name="modelToInstantiate">Reference to the model, which will be re-instantiated.</param>
    /// <returns></returns>
    private void StartVivianFramework(string url, GameObject modelToInstantiate)
    {
        // Remove outlines on copied model in case there were any
        foreach (Outline child in modelToInstantiate.transform.GetChild(0).GetChild(0).GetComponentsInChildren<Outline>()) {
            Destroy(child);
        }
        // Backup the TriLib model as it is
        _modelBackup = Instantiate(modelToInstantiate);
        _modelBackup.SetActive(false);
        // Give a collider-less model to Vivian so that it creates its own instead of reusing existing ones
        // This fixes isTrigger errors on concave mesh colliders
        foreach (Transform child in modelToInstantiate.transform.GetChild(0).GetChild(0).GetComponentsInChildren<Transform>()) {
            Destroy(child.gameObject.GetComponent<MeshCollider>());
        }

        _vivianInstance = Instantiate(vivianPrefab);
        var vp = _vivianInstance.GetComponent<VirtualPrototype>();
        vp.BundleURL = url;
        vp.virtualPrototypePrefab = modelToInstantiate;
        // Set model as child of Vivian so that it directly uses it instead or re-instantiating
        modelToInstantiate.transform.parent = _vivianInstance.transform;
    }

    /// <summary>
    /// Toggles the Vivian interaction Game Object and disables the configured TriLib equivalent. 
    /// </summary>
    private IEnumerator SwitchManipulation()
    {
        yield return new WaitUntil(() => ModelManipulator.Initialized);
        _vivianInteraction.SetActive(!_vivianInteraction.activeSelf);
        ModelManipulator.Unselect();
        ModelManipulator.enabled = !ModelManipulator.enabled;
    }
    
    /// <summary>
    /// Gets the correct Vivian manipulation depending on the current reality
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    private void GetVivianInteraction()
    {
        Enum.TryParse(XRUI.GetCurrentReality(), true, out XRUI.RealityType reality);
        switch (reality)
        {
            case XRUI.RealityType.PC:
                _vivianInteraction = GameObject.Find("MouseInteraction");
                break;
            case XRUI.RealityType.AR:
                _vivianInteraction = GameObject.Find("ARInteraction");
                break;      
            case XRUI.RealityType.VR:
                _vivianInteraction = GameObject.Find("VRInteraction");
                break;
        }

        if (_vivianInteraction == null)
        {
            throw new ArgumentException($"No Vivian interaction prefab could be found for the current reality ({reality.ToString()})");
        }
    }
}
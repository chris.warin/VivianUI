using UnityEngine;
using UnityEngine.UIElements;

namespace VivianComponents
{
    public class VivianComponent : MonoBehaviour
    {
        public VisualElement VisualElement
        {
            get
            {
                return _visualElement;
            }
            set
            {
                _visualElement = value;
                OnComponentUpdated();
            }
        }

        public Utils.ElementType ElementType;
        protected ElementPicker ElementPicker;
        private VisualElement _visualElement;

        protected void Start()
        {
            ElementPicker = GetComponent<ElementPicker>();
            ElementPicker.ChangeVivianElement(VisualElement, ElementType);
            OnComponentUpdated();
        }

        public virtual void OnComponentUpdated()
        {
            
        }
    }
}
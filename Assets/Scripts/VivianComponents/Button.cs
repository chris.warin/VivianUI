using System.Collections.Generic;
using System.Linq;
using de.ugoe.cs.vivian.core;
using UnityEngine.UIElements;

namespace VivianComponents
{
    public class Button : VivianComponent
    {
        private UnityEngine.UIElements.Button _newSituationBtn;
        private UnityEngine.UIElements.Button _existingSituationBtn;
        private DropdownField _dropdown;
        private List<string> _situations;

        private TransitionSpec _transition;
        private InteractionElementSpec _spec;

        private void CreateSituation()
        {
            ShowDropdown();
            FindObjectOfType<SituationsList>().AddSituationUI("New Situation", false, false, true);
        }

        private void ShowDropdown()
        {
            ElementPicker.UpdateVivianElementTemplate(Utils.ElementType.BUTTON, "Button_Dropdown");
            ElementPicker.ChangeVivianElement(VisualElement, Utils.ElementType.BUTTON);
            _dropdown = VisualElement.Q<DropdownField>("Dropdown");
            _dropdown.choices = _situations;
            _dropdown.index = 0;

            // Add callback when selection changes
            _dropdown.RegisterValueChangedCallback(CreateOrUpdateButtonTransition);
            
            VivianUI.Instance.SituationsList.RegisterCallbackOnSituationCreation(() =>
            {
                _dropdown.choices = _situations;
                _dropdown.choices.Add(VivianUI.States[^1].Name);
                _dropdown.index = _situations.Count - 1;
            }, true);
        }

        private void CreateOrUpdateButtonTransition(ChangeEvent<string> evt = null)
        {
            var originSituation = Utils.GetCurrentState();
            var destinationSituation = Utils.GetState(_dropdown.value);

            _spec = new ButtonSpec(FindObjectOfType<ModelManipulator>().SelectedElement?.name);
            var oldTransition = Utils.GetTransition(originSituation.Name, _spec.Name, EventSpec.BUTTON_PRESS);
            if(oldTransition is not null)
                Utils.DeleteTransition(oldTransition);
            
            // If the dropdown was set to "No change", don't create a new transition
            if (destinationSituation is not null)
            {
                _transition = new TransitionSpec(originSituation, _spec, EventSpec.BUTTON_PRESS, destinationSituation);
                VivianUI.Transitions.Add(_transition);
            };
        }

        public override void OnComponentUpdated()
        {
            // Feed the dropdown with the situation list
            _situations = new List<string>() {"No change"};
            _situations.AddRange(VivianUI.States.Select(s => s.Name).ToList());
            _situations.Remove(Utils.GetCurrentState().Name);
            _newSituationBtn = VisualElement.Q<UnityEngine.UIElements.Button>("NewSituation");
            _existingSituationBtn = VisualElement.Q<UnityEngine.UIElements.Button>("ExistingSituation");
            _dropdown = VisualElement.Q<DropdownField>("Dropdown");

            // If on initial page
            if (_newSituationBtn is not null)
            {
                _newSituationBtn.clicked += CreateSituation;
                _existingSituationBtn.clicked += ShowDropdown;
            }
            // if on dropdown page
            if (_dropdown is not null)
            {
                _dropdown.choices = _situations;
                var transition = Utils.GetTransition(Utils.GetCurrentState().Name, 
                    FindObjectOfType<ModelManipulator>().SelectedElement?.name, EventSpec.BUTTON_PRESS);
                if (transition is not null)
                {
                    _dropdown.index = _situations.FindIndex(s => s.Equals(transition.DestinationState.Name));
                }
                else
                    _dropdown.index = 0;
                
                // Add callback when selection changes
                _dropdown.RegisterValueChangedCallback(CreateOrUpdateButtonTransition);
            }
        }
    }
}
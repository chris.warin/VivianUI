using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using com.chwar.xrui;
using de.ugoe.cs.vivian.core;
using Newtonsoft.Json;
using TriLib;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectFilesController : MonoBehaviour
{
    public string InternalProjectPath => _internalUnityProjectPath;

    // Project name
    private string _projectName;
    // Project paths
    private string _internalUnityProjectPath;
    private string _externalProjectPath;
    private string _specDir;
    private string _screenDir;
    
    // List of model paths and GOs
    private List<string> _modelPaths;
    
    // Functional Specification JSON paths
    private string _interactionElementsJsonPath;
    private string _visualizationElementsJsonPath;
    private string _statesJsonPath;
    private string _transitionsJsonPath;
    private string _visualizationArraysJsonPath;

    /// <summary>
    /// Creates a new project. Creates a folder with uploaded model along with an empty functional specification at Unity's persistent data path.
    /// Duplicates the internal project to the indicated path.
    /// </summary>
    /// <param name="projectName">Name of the project to create.</param>
    /// <param name="projectPath">Path of the project.</param>
    /// <param name="modelPaths">Path of the model(s) to import.</param>
    /// <param name="createSubdir">Whether to create a subdirectory in the at the given path.</param>
    /// <returns>The loaded model's Game Object</returns>
    public async Task<List<GameObject>> CreateFiles(string projectName, string projectPath, List<string> modelPaths, bool createSubdir)
    {
        GetPaths(projectName, projectPath, createSubdir);

        // Create project folders inside Unity
        Directory.CreateDirectory(_internalUnityProjectPath);
        Directory.CreateDirectory(_specDir);
        Directory.CreateDirectory(_screenDir);
        // Create Specification Files
        await File.WriteAllTextAsync(_interactionElementsJsonPath, "{\"Elements\":[]}");
        await File.WriteAllTextAsync(_statesJsonPath, "{\"States\":[]}");
        await File.WriteAllTextAsync(_transitionsJsonPath, "{\"Transitions\":[]}");
        await File.WriteAllTextAsync(_visualizationElementsJsonPath, "{\"Elements\":[]}");
        await File.WriteAllTextAsync(_visualizationArraysJsonPath, "{\"Elements\":[]}");

        // Copy original models to project folder
        List<GameObject> listModels = new();
        _modelPaths = new();
        foreach (var path in modelPaths)
        {
            string modelName = path.Split(@"\"[0], "/"[0]).Last();
            string newModelPath = Path.Combine(_internalUnityProjectPath, modelName);
            
            // Check if the model path is an URL
            if (path.StartsWith("http"))
                await DownloadModel(path, newModelPath);
            else
                File.Copy(path, newModelPath, true);
            
            GameObject model = LoadModel(newModelPath);
            listModels.Add(model);
            _modelPaths.Add(newModelPath);
        }

        // Copy internal folder to project path
        //SaveProject();
        
        // Save info to PlayerPrefs
        List<ProjectJsonWrapper> projects = GetRecentProjects();
        //RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, -1);
        RuntimePreviewGenerator.MarkTextureNonReadable = false;
        Texture2D thumbnail = RuntimePreviewGenerator.GenerateModelPreview(listModels[0].transform);
        var currentProject = new ProjectJsonWrapper
        {
            projectName = _projectName,
            projectPath = _externalProjectPath,
            projectSubdir = createSubdir,
            modelPaths = _modelPaths,
            lastEdit = "Last edited: " + DateTime.Now.ToString(CultureInfo.InvariantCulture),
            base64Screenshot = thumbnail.EncodeToPNG()
        };
        projects.Add(currentProject);
        SetRecentProjects(projects);
        Destroy(thumbnail);
        
        return listModels;
    }
    
    /// <summary>
    /// Loads the required files for a project.
    /// </summary>
    /// <param name="projectName">Name of the project to load.</param>
    /// <param name="projectPath">Path of the project.</param>
    /// <param name="modelPaths">Path of the model(s) to import.</param>
    /// <param name="createSubdir">Whether to create a subdirectory in the at the given path.</param>
    /// <returns></returns>
    public List<GameObject> LoadFiles(string projectName, string projectPath, List<string> modelPaths, bool createSubdir)
    {
        GetPaths(projectName, projectPath, createSubdir);
        LoadVivianSpecificationFromDisk();
        List<GameObject> modelsList = new List<GameObject>();
        _modelPaths = modelPaths;
        foreach (var path in modelPaths)
        {
            GameObject model = LoadModel(path);
            modelsList.Add(model);
        }
        return modelsList;
    }

    /// <summary>
    /// Saves the project to the project's path by duplicating the internal project's folder
    /// </summary>
    public void SaveProject()
    {
        SaveVivianSpecificationToDisk();
        
        // Create directories
        foreach (string dirPath in Directory.GetDirectories(_internalUnityProjectPath, "*", SearchOption.AllDirectories))
        {
            Directory.CreateDirectory(dirPath.Replace(_internalUnityProjectPath, _externalProjectPath));
        }

        // Copy all the files & Replaces any files with the same name
        foreach (string newPath in Directory.GetFiles(_internalUnityProjectPath, "*.*",SearchOption.AllDirectories))
        {
            File.Copy(newPath, newPath.Replace(_internalUnityProjectPath, _externalProjectPath), true);
        }
        
        List<ProjectJsonWrapper> projects = GetRecentProjects();
        ProjectJsonWrapper currentProject = projects.Find(p => p.projectName.Equals(_projectName));
        if (currentProject != null)
        {
            currentProject.lastEdit = "Last edited: " + DateTime.Now.ToString(CultureInfo.InvariantCulture);
            currentProject.modelPaths = _modelPaths;
            currentProject.specsUI = VivianUI.Instance.ElementPicker.SpecsUI;
            SetRecentProjects(projects);
        }
    }

    private async Task DownloadModel(string url, string pathToSave)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            webRequest.downloadHandler = new DownloadHandlerFile(pathToSave);
            
            // Request and dl the model.
            await webRequest.SendWebRequest();
            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                // GameObject model = LoadModel(newModelPath);
                // _listModels.Add(model);
            }
            else
            {
                XRUI.Instance.ShowAlert(XRUI.AlertType.Danger, "Error","The model could not be downloaded. Please check the URL.");
            }
        }
    }

    private void GetPaths(string projectName, string projectPath, bool createSubdir)
    {
        // Set current project name
        _projectName = projectName;
        // Determine paths
        _internalUnityProjectPath = Path.Combine(Application.persistentDataPath, projectName);
        _externalProjectPath = createSubdir ? Path.Combine(projectPath, projectName) : projectPath;
        
        _specDir = Path.Combine(_internalUnityProjectPath,  "FunctionalSpecification");
        _screenDir = Path.Combine(_internalUnityProjectPath,  "Screens");

        _interactionElementsJsonPath = Path.Combine(_specDir, "InteractionElements.json");
        _statesJsonPath = Path.Combine(_specDir, "States.json");
        _transitionsJsonPath = Path.Combine(_specDir, "Transitions.json");
        _visualizationElementsJsonPath = Path.Combine(_specDir, "VisualizationElements.json");
        _visualizationArraysJsonPath = Path.Combine(_specDir, "VisualizationArrays.json");
    }

    /// <summary>
    /// Loads all Vivian specification from JSON files stored in the project folder
    /// </summary>
    private void LoadVivianSpecificationFromDisk()
    {
        try
        {
            // Read Interaction Elements JSON from file
            List<InteractionElementSpec> interactionElements = JsonUtility
                .FromJson<InteractionElementSpecArrayJSONWrapper>
                    (File.ReadAllText(_interactionElementsJsonPath)).GetSpecsArray().ToList();
            // Read VisualizationElements JSON from file
            List<VisualizationElementSpec> visualizationElements = JsonUtility
                .FromJson<VisualizationElementSpecArrayJSONWrapper>
                    (File.ReadAllText(_visualizationElementsJsonPath)).GetSpecsArray().ToList();
            // Read States JSON from file
            List<StateSpec> states = JsonUtility.FromJson<StateSpecArrayJSONWrapper>
                    (File.ReadAllText(_statesJsonPath))
                .GetSpecsArray(interactionElements.ToArray(), visualizationElements.ToArray()).ToList();
            // Read Transitions JSON from file
            List<TransitionSpec> transitions = JsonUtility.FromJson<TransitionSpecArrayJSONWrapper>
                    (File.ReadAllText(_transitionsJsonPath))
                .GetSpecsArray(states.ToArray(), interactionElements.ToArray())
                .ToList();
            // Read VisualizationArrays JSON from file
            List<VisualizationArraySpec> visualizationArrays = JsonUtility
                .FromJson<VisualizationArraySpecArrayJSONWrapper>
                    (File.ReadAllText(_visualizationArraysJsonPath)).GetSpecsArray(visualizationElements.ToArray())
                .ToList();
            
            VivianUI.InitArrays();
            VivianUI.InteractionElements.AddRange(interactionElements);
            VivianUI.VisualizationElements.AddRange(visualizationElements);
            VivianUI.States.AddRange(states);
            VivianUI.Transitions.AddRange(transitions);
            VivianUI.VisualizationArrays.AddRange(visualizationArrays);
        }
        catch (ArgumentException e)
        {
            XRUI.Instance.ShowAlert(XRUI.AlertType.Danger, "Could not load project", $"Error: {e.Message}");
            throw;
        }
    }

    /// <summary>
    /// Saves all Vivian specification to JSON files in the project folder
    /// </summary>
    public void SaveVivianSpecificationToDisk()
    {
        InteractionElementSpecArrayJSONWrapper interactionElementSpecArrayJsonWrapper =
            new InteractionElementSpecArrayJSONWrapper(VivianUI.InteractionElements.ToArray());
        VisualizationElementSpecArrayJSONWrapper visualizationElementSpecArrayJsonWrapper =
            new VisualizationElementSpecArrayJSONWrapper(VivianUI.VisualizationElements.ToArray());
        StateSpecArrayJSONWrapper stateSpecArrayJsonWrapper =
            new StateSpecArrayJSONWrapper(VivianUI.States.ToArray());
        TransitionSpecArrayJSONWrapper transitionSpecArrayJsonWrapper =
            new TransitionSpecArrayJSONWrapper(VivianUI.Transitions.ToArray());
        VisualizationArraySpecArrayJSONWrapper visualizationArraySpecArrayJsonWrapper =
            new VisualizationArraySpecArrayJSONWrapper(VivianUI.VisualizationArrays.ToArray());
            
        string interactionElements = JsonUtility.ToJson(interactionElementSpecArrayJsonWrapper, true);
        string visualizationElements = JsonUtility.ToJson(visualizationElementSpecArrayJsonWrapper, true);
        string states = JsonUtility.ToJson(stateSpecArrayJsonWrapper, true);
        string transitions = JsonUtility.ToJson(transitionSpecArrayJsonWrapper, true);
        string visualizationArrays = JsonUtility.ToJson(visualizationArraySpecArrayJsonWrapper, true);
        
        File.WriteAllText(_interactionElementsJsonPath, interactionElements);
        File.WriteAllText(_visualizationElementsJsonPath, visualizationElements);
        File.WriteAllText(_statesJsonPath, states);
        File.WriteAllText(_transitionsJsonPath, transitions);
        File.WriteAllText(_visualizationArraysJsonPath, visualizationArrays);
    }
    
    /// <summary>
    /// Loads a model with TriLib
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns>The loaded Game Object</returns>
    public GameObject LoadModel(string filePath)
    {
        using var assetLoader = new AssetLoader();
        //Creates the AssetLoaderOptions instance.
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();   
        assetLoaderOptions.Scale = 0.01f;
        assetLoaderOptions.UseOriginalPositionRotationAndScale = true;
        //Loads the model synchronously and stores the reference in myGameObject.
        return assetLoader.LoadFromFile(filePath, assetLoaderOptions);
    }

    /// <summary>
    /// Gets the VivianUI projects from the Unity PlayerPrefs.
    /// </summary>
    /// <returns></returns>
    public List<ProjectJsonWrapper> GetRecentProjects()
    {
        var json = PlayerPrefs.GetString("projects");
        var res = JsonConvert.DeserializeObject<ProjectsJsonWrapper>(json);
        return res?.projects ?? new List<ProjectJsonWrapper>();
    }

    /// <summary>
    /// Sets the VivianUI projects list in the Unity PlayerPrefs.
    /// </summary>
    /// <param name="projects"></param>
    public void SetRecentProjects(List<ProjectJsonWrapper> projects)
    {
        ProjectsJsonWrapper wrapper = new ProjectsJsonWrapper {projects = projects};
        var json = JsonConvert.SerializeObject(wrapper);
        PlayerPrefs.SetString("projects", json);
    }
}

[Serializable]
public class ProjectsJsonWrapper
{
    public List<ProjectJsonWrapper> projects;
}

[Serializable]
public class ProjectJsonWrapper
{
    public string projectName;
    public string projectPath;
    public bool projectSubdir;
    public List<string> modelPaths;
    public string lastEdit;
    public byte[] base64Screenshot;
    public Dictionary<string, List<InspectorSpecsUI>> specsUI;
}
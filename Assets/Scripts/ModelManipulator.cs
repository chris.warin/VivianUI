using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.chwar.xrui.UIElements;
using UnityEngine;

public class ModelManipulator : MonoBehaviour
{
    public Transform Target { get; private set; }
    public GameObject SelectedElement  { get; private set; }
    
    public bool Initialized { get; private set; }
    
    [SerializeField]
    private XRUICard cardElementPicker;
    private ElementPicker _elementPicker;
    private Outliner _outliner;
    private Camera _camera;
    private Controls _controls;
    private Vector3 _previousPosition;
    private Dictionary<string, List<RaycastHit>> _hitsList;
    private List<RaycastHit>.Enumerator _listEnum;
    private bool _targetReady;


    private void Awake()
    {
        _controls = new Controls();
        _camera = Camera.main;
    }

    private void OnEnable()
    {
        _controls.Enable();
        _camera.transform.LookAt(Target);
    }

    private void OnDisable()
    {
        _controls.Disable();
    }

    IEnumerator Start()
    {
        // Wait until target is ready
        yield return new WaitUntil(() => _targetReady); 
        
        _elementPicker = cardElementPicker.GetComponent<ElementPicker>();
        _outliner = GetComponent<Outliner>();
        _hitsList = new Dictionary<string, List<RaycastHit>>();

        // Hide Card
        yield return new WaitUntil(() => cardElementPicker.isActiveAndEnabled);
        cardElementPicker.Show(false);

        _controls.Player.Look.started += _ => SavePosition();
        _controls.Player.Look.performed += _ => RotateAroundModel();
        _controls.Player.Select.performed += _ => SelectElement();
        _controls.Player.Zoom.performed += _ => Zoom();

        Initialized = true;
    }

    private void SavePosition()
    {
        var deltaPointer = _controls.Player.Look.ReadValue<Vector2>();
        _previousPosition = _camera.ScreenToViewportPoint(deltaPointer);
    }

    /// <summary>
    /// Zooms or un-zooms the camera with respect to the target. 
    /// </summary>
    private void Zoom()
    {
        if(!cardElementPicker.PointerOverUI)
            _camera.transform.Translate(_camera.transform.forward * -_controls.Player.Zoom.ReadValue<float>() * .1f, Target.transform);
    }

    /// <summary>
    /// Rotates the camera around the targeted model.
    /// </summary>
    private void RotateAroundModel()
    {
        var targetPos = Target.transform.position;
        var distanceToTarget = Vector3.Distance(_camera.transform.position, targetPos);
        Vector3 newPosition = _camera.ScreenToViewportPoint(_controls.Player.Look.ReadValue<Vector2>());
        Vector3 direction = _previousPosition - newPosition;

        _camera.transform.position = targetPos;
        _camera.transform.Rotate(Vector3.right, direction.y * 180);
        _camera.transform.Rotate(Vector3.up, -direction.x * 180, Space.World);
        _camera.transform.Translate(new Vector3(0, 0, -distanceToTarget));
        _previousPosition = newPosition;
    }

    private void SelectElement()
    {
        if (Camera.main is not null)
        {
            Ray ray = Camera.main.ScreenPointToRay(_controls.Player.Select.ReadValue<Vector2>());
            if (Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                if(SelectedElement != null && SelectedElement.Equals(hitInfo.collider.gameObject) && !Utils.IsPointerOverUI())
                {
                    GetNextHit();
                }
                else
                {
                    Select(hitInfo);
                    RaycastHit[] hits = new RaycastHit[5];
                    Physics.RaycastNonAlloc(ray, hits);
                    if(!_hitsList.ContainsKey(hitInfo.collider.name))
                    {
                        _hitsList.Add(hitInfo.collider.name, null);
                    }
                    // Override the list to keep consistency with current ray
                    _hitsList[hitInfo.collider.name] = hits.ToList();
                    _listEnum = _hitsList[hitInfo.collider.name].GetEnumerator();
                }
            }
            else if(!Utils.IsPointerOverUI())
            {
                Unselect();
            }
        }
    }

    /// <summary>
    /// Recursive function that iterates through the list of hits related to the currently selected element.
    /// </summary>
    private void GetNextHit()
    {
        if (_listEnum.MoveNext())
        {
            if(_listEnum.Current.collider != null && _listEnum.Current.collider.gameObject != SelectedElement)
                Select(_listEnum.Current);
            else
                GetNextHit();
        } else
            Unselect();
    }

    /// <summary>
    /// Selects an element that was clicked.
    /// </summary>
    /// <param name="hitInfo"></param>
    private void Select(RaycastHit hitInfo)
    {
        // Feed the name of the clicked object to the Element Picker 
        SelectedElement = hitInfo.collider.gameObject;
        _elementPicker.UpdateElementPicker(SelectedElement.name);
        cardElementPicker.Show(true);
        cardElementPicker.UpdateTitle(SelectedElement.name);
        _outliner.RemoveAllOutlines();
        _outliner.SetOutline(SelectedElement, "deep-orange");
    }

    /// <summary>
    /// Unselects an element.
    /// </summary>
    public void Unselect()
    {
        SelectedElement = null;
        cardElementPicker.Show(false);
        _outliner?.RemoveAllOutlines();
    }

    /// <summary>
    /// Defines the target that the camera should aim at or rotate around.
    /// </summary>
    /// <param name="obj">The target to set.</param>
    public void SetTarget(GameObject obj)
    {
        Target = obj.transform;
        _targetReady = true;
    }
}

﻿// This file was taken from the original Vivifly code by Nico Funke.

using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Outliner : MonoBehaviour {

    /// <summary>
    /// Maps GameObjects with their corresponding outline component
    /// </summary>
    private readonly Dictionary<GameObject, cakeslice.Outline> _outlineComponents = new();

    /// <summary>
    /// Outlines a game object
    /// </summary>
    /// <param name="objectToOutline">GameObject that should be outlined</param>
    /// <param name="colorName">red, deep-orange, light-green or cyan</param>
    public void SetOutline(GameObject objectToOutline, string colorName) {
        int colorCode = this.ColorNameToCode(colorName);

        if (_outlineComponents.ContainsKey(objectToOutline)) {
            // Object is already outlined -> Just change color
            _outlineComponents[objectToOutline].color = colorCode;
        }
        else {
            // create new outline component
            cakeslice.Outline outlineComponent = objectToOutline.AddComponent<cakeslice.Outline>();
            outlineComponent.color = colorCode;
            _outlineComponents.Add(objectToOutline, outlineComponent);
        }
    }

    /// <summary>
    /// Removes the outline of an gameObject
    /// </summary>
    /// <param name="outlinedObject">Outlined GameObject</param>
    public void RemoveOutline(GameObject outlinedObject) {
        if (!_outlineComponents.ContainsKey(outlinedObject)) {
            // Error: Object is not outlined
            return;
        }
        Destroy(_outlineComponents[outlinedObject]);
        _outlineComponents.Remove(outlinedObject);
    }

    /// <summary>
    /// Removes outlines of all gameObjects
    /// </summary>
    public void RemoveAllOutlines() {
        GameObject[] keyObjects = _outlineComponents.Keys.ToArray();
        foreach (GameObject keyObject in keyObjects) {
            this.RemoveOutline(keyObject);
        }
    }

    /// <summary>
    /// returns the matching color number of the outline colors for a given color name or -1 if the color does not exist
    /// </summary>
    /// <param name="colorName">red, deep-orange, light-green or cyan</param>
    /// <returns></returns>
    private int ColorNameToCode(string colorName) {
        switch (colorName) {
            case "red":
                return 0;
            case "light-green":
                return 1;
            case "deep-orange":
                return 2;
            case "cyan":
                return 3;
            default:
                return -1;
        }
    }
}

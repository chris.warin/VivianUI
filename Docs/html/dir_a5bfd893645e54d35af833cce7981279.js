var dir_a5bfd893645e54d35af833cce7981279 =
[
    [ "InternalData", "dir_8b2e82cbde8aee8a86eebc31a93eac12.html", "dir_8b2e82cbde8aee8a86eebc31a93eac12" ],
    [ "AssetAdvancedConfigs.cs", "_asset_advanced_configs_8cs.html", "_asset_advanced_configs_8cs" ],
    [ "AssetDownloader.cs", "_asset_downloader_8cs.html", [
      [ "TriLib.AssetDownloader", "class_tri_lib_1_1_asset_downloader.html", "class_tri_lib_1_1_asset_downloader" ]
    ] ],
    [ "AssetLoader.cs", "_asset_loader_8cs.html", [
      [ "TriLib.AssetLoader", "class_tri_lib_1_1_asset_loader.html", "class_tri_lib_1_1_asset_loader" ]
    ] ],
    [ "AssetLoaderAsync.cs", "_asset_loader_async_8cs.html", [
      [ "TriLib.AssetLoaderAsync", "class_tri_lib_1_1_asset_loader_async.html", "class_tri_lib_1_1_asset_loader_async" ]
    ] ],
    [ "AssetLoaderBase.cs", "_asset_loader_base_8cs.html", "_asset_loader_base_8cs" ],
    [ "AssetLoaderOptions.cs", "_asset_loader_options_8cs.html", [
      [ "TriLib.AssetLoaderOptions", "class_tri_lib_1_1_asset_loader_options.html", "class_tri_lib_1_1_asset_loader_options" ]
    ] ],
    [ "AssetLoaderZip.cs", "_asset_loader_zip_8cs.html", [
      [ "TriLib.AssetLoaderZip", "class_tri_lib_1_1_asset_loader_zip.html", "class_tri_lib_1_1_asset_loader_zip" ]
    ] ],
    [ "AssetUnloader.cs", "_asset_unloader_8cs.html", [
      [ "TriLib.AssetUnloader", "class_tri_lib_1_1_asset_unloader.html", "class_tri_lib_1_1_asset_unloader" ]
    ] ],
    [ "AssimpConfigs.cs", "_assimp_configs_8cs.html", "_assimp_configs_8cs" ],
    [ "AssimpInterop.cs", "_assimp_interop_8cs.html", [
      [ "TriLib.AssimpInterop", "class_tri_lib_1_1_assimp_interop.html", "class_tri_lib_1_1_assimp_interop" ]
    ] ],
    [ "AssimpMetadataCollection.cs", "_assimp_metadata_collection_8cs.html", [
      [ "TriLib.AssimpMetadataCollection", "class_tri_lib_1_1_assimp_metadata_collection.html", "class_tri_lib_1_1_assimp_metadata_collection" ]
    ] ],
    [ "MaterialShadingMode.cs", "_material_shading_mode_8cs.html", "_material_shading_mode_8cs" ],
    [ "STBImageInterop.cs", "_s_t_b_image_interop_8cs.html", [
      [ "STBImage.STBImageInterop", "class_s_t_b_image_1_1_s_t_b_image_interop.html", "class_s_t_b_image_1_1_s_t_b_image_interop" ]
    ] ],
    [ "STBImageLoader.cs", "_s_t_b_image_loader_8cs.html", [
      [ "STB.STBImageLoader", "class_s_t_b_1_1_s_t_b_image_loader.html", "class_s_t_b_1_1_s_t_b_image_loader" ]
    ] ]
];
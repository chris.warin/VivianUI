var class_project_files_controller =
[
    [ "CreateFiles", "class_project_files_controller.html#adfb41e1c1eb0d1ed4c610f6ef147395c", null ],
    [ "GetRecentProjects", "class_project_files_controller.html#afa69e307a9f3ba06bc8b4021211c4c9b", null ],
    [ "LoadFiles", "class_project_files_controller.html#a7889a7b40c3bc7b4ef5f838fa8f33c12", null ],
    [ "LoadModel", "class_project_files_controller.html#a668edb7be8d74a0f3b93d9577248a735", null ],
    [ "SaveProject", "class_project_files_controller.html#aed7f6eed3c917e6bc4e5028212e7d2a1", null ],
    [ "SaveVivianSpecificationToDisk", "class_project_files_controller.html#a465eedff13384dab7e334c57e6d88a1d", null ],
    [ "SetRecentProjects", "class_project_files_controller.html#adaa01697d0900cf78f411a18dc7090da", null ],
    [ "InternalProjectPath", "class_project_files_controller.html#ab28f3fe1f617cb46ad9b75eab3298a78", null ]
];
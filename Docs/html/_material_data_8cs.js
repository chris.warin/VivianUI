var _material_data_8cs =
[
    [ "TriLib.IMaterialProperty", "interface_tri_lib_1_1_i_material_property.html", "interface_tri_lib_1_1_i_material_property" ],
    [ "TriLib.MaterialProperty< T >", "class_tri_lib_1_1_material_property.html", "class_tri_lib_1_1_material_property" ],
    [ "TriLib.MaterialData", "class_tri_lib_1_1_material_data.html", "class_tri_lib_1_1_material_data" ],
    [ "aiPropertyTypeInfo", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266", [
      [ "aiPTI_Float", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266a6edab39ff3cf3f56bb3625d022f1726a", null ],
      [ "aiPTI_Double", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266ae285fb2de372baa6c01145447a3ad270", null ],
      [ "aiPTI_String", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266a1efac4c9a105edbe522de79bf9fc13af", null ],
      [ "aiPTI_Integer", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266a3e226e23489c98c070086328fef7108c", null ],
      [ "aiPTI_Buffer", "_material_data_8cs.html#aa5a33be19cdfd9c8595de4d258c1f266adaa7ef48603b2abf0f6a4a23c3444bbc", null ]
    ] ]
];
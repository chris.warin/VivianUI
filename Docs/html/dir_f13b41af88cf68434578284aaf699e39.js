var dir_f13b41af88cf68434578284aaf699e39 =
[
    [ "Modals", "dir_957911e51f7d56561344a4fd676ee3e7.html", "dir_957911e51f7d56561344a4fd676ee3e7" ],
    [ "VivianComponents", "dir_02e71e2a16d134a07c24a1a8925406a6.html", "dir_02e71e2a16d134a07c24a1a8925406a6" ],
    [ "Controls.cs", "_controls_8cs.html", [
      [ "Controls", "class_controls.html", "class_controls" ],
      [ "Controls.PlayerActions", "struct_controls_1_1_player_actions.html", "struct_controls_1_1_player_actions" ],
      [ "Controls.IPlayerActions", "interface_controls_1_1_i_player_actions.html", "interface_controls_1_1_i_player_actions" ]
    ] ],
    [ "ElementPicker.cs", "_element_picker_8cs.html", "_element_picker_8cs" ],
    [ "ElementVivianSpecs.cs", "_element_vivian_specs_8cs.html", [
      [ "ElementVivianSpecs", "class_element_vivian_specs.html", null ]
    ] ],
    [ "Menu.cs", "_menu_8cs.html", [
      [ "Menu", "class_menu.html", "class_menu" ]
    ] ],
    [ "ModelManipulator.cs", "_model_manipulator_8cs.html", [
      [ "ModelManipulator", "class_model_manipulator.html", "class_model_manipulator" ]
    ] ],
    [ "Navbar.cs", "_navbar_8cs.html", [
      [ "Navbar", "class_navbar.html", "class_navbar" ]
    ] ],
    [ "Outliner.cs", "_outliner_8cs.html", [
      [ "Outliner", "class_outliner.html", "class_outliner" ]
    ] ],
    [ "ProjectFilesController.cs", "_project_files_controller_8cs.html", [
      [ "ProjectFilesController", "class_project_files_controller.html", "class_project_files_controller" ],
      [ "ProjectsJsonWrapper", "class_projects_json_wrapper.html", "class_projects_json_wrapper" ],
      [ "ProjectJsonWrapper", "class_project_json_wrapper.html", "class_project_json_wrapper" ]
    ] ],
    [ "SituationsList.cs", "_situations_list_8cs.html", "_situations_list_8cs" ],
    [ "Utils.cs", "_utils_8cs.html", [
      [ "Utils", "class_utils.html", "class_utils" ],
      [ "UnityWebRequestAwaiter", "struct_unity_web_request_awaiter.html", "struct_unity_web_request_awaiter" ],
      [ "ExtensionMethods", "class_extension_methods.html", "class_extension_methods" ],
      [ "ContextualMenuEntries", "struct_contextual_menu_entries.html", "struct_contextual_menu_entries" ]
    ] ],
    [ "VivianUI.cs", "_vivian_u_i_8cs.html", [
      [ "VivianUI", "class_vivian_u_i.html", "class_vivian_u_i" ]
    ] ]
];
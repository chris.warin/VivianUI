var _assimp_metadata_8cs =
[
    [ "TriLib.AssimpMetadata", "class_tri_lib_1_1_assimp_metadata.html", "class_tri_lib_1_1_assimp_metadata" ],
    [ "AssimpMetadataType", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cd", [
      [ "AI_BOOL", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda71ded29da8a4515bfd19538042524549", null ],
      [ "AI_INT32", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda8784f61ba369afb4c631fef5d7530ae2", null ],
      [ "AI_UINT64", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda21cc571bfc89240c4066f08046c0b737", null ],
      [ "AI_FLOAT", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cdaef56c9b5c99c05324227174f9142832a", null ],
      [ "AI_DOUBLE", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda1dc0ca532814178cbf1708a63c03870a", null ],
      [ "AI_AISTRING", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda2448855dc2a47de07ad4dbd1698afd5e", null ],
      [ "AI_AIVECTOR3D", "_assimp_metadata_8cs.html#a24138cb9cc4ad8f52e30b59bb3cc04cda8d3a05b25642914cb28b3a695d6dae8e", null ]
    ] ]
];
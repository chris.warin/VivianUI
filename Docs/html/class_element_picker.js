var class_element_picker =
[
    [ "ChangeVivianElement", "class_element_picker.html#a7bace01ca4ab5ddf4cb80df543fd9e9e", null ],
    [ "RemoveAllVivianComponents", "class_element_picker.html#acd3b224946b891ac45d91ed7f3938017", null ],
    [ "UpdateElementPicker", "class_element_picker.html#af7581458cf7547f28593a35c6bbcb049", null ],
    [ "UpdateVivianElementTemplate", "class_element_picker.html#a925765ebdc7b333d74d3dd75560768c5", null ],
    [ "listSpecs", "class_element_picker.html#adb1b8e63a78453c3fba3eb27b4eabf4e", null ],
    [ "mainText", "class_element_picker.html#abdb433913ce16ff3ae297b0340a186df", null ],
    [ "SpecsUI", "class_element_picker.html#afe6090ae3f7a7cf71ac0b5d2f18e2858", null ]
];
var _material_shading_mode_8cs =
[
    [ "MaterialShadingMode", "_material_shading_mode_8cs.html#a4739993bd8f65cb2673a91829f9574a7", [
      [ "Standard", "_material_shading_mode_8cs.html#a4739993bd8f65cb2673a91829f9574a7aeb6d8ae6f20283755b339c0dc273988b", null ],
      [ "Roughness", "_material_shading_mode_8cs.html#a4739993bd8f65cb2673a91829f9574a7a767078b82d60bc9dfaec29326b0b7c06", null ],
      [ "Specular", "_material_shading_mode_8cs.html#a4739993bd8f65cb2673a91829f9574a7a39b0044dd8789d333e7794f359406740", null ]
    ] ],
    [ "MaterialTransparencyMode", "_material_shading_mode_8cs.html#ab1a92871b434f66d43eba635dfd45f77", [
      [ "Alpha", "_material_shading_mode_8cs.html#ab1a92871b434f66d43eba635dfd45f77a6132295fcf5570fb8b0a944ef322a598", null ],
      [ "Cutout", "_material_shading_mode_8cs.html#ab1a92871b434f66d43eba635dfd45f77a330be5af6c8bafc8ce5c74fa208c5015", null ],
      [ "Fade", "_material_shading_mode_8cs.html#ab1a92871b434f66d43eba635dfd45f77a04e0385c10aefee8e4681617d2f3ef40", null ]
    ] ]
];
var _assimp_configs_8cs =
[
    [ "TriLib.AssimpProcessPreset", "class_tri_lib_1_1_assimp_process_preset.html", "class_tri_lib_1_1_assimp_process_preset" ],
    [ "AssimpPostProcessSteps", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39", [
      [ "CalcTangentSpace", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a3bce9359b1db4d15c03000c66e0b0d52", null ],
      [ "JoinIdenticalVertices", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a4642a6f7959b17c1b5f093a62e4a688c", null ],
      [ "MakeLeftHanded", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39aba1741832a48fbab3ee1849d2623c73a", null ],
      [ "Triangulate", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a7862a2cb2f6887e78b0bce4f6e84e7ac", null ],
      [ "RemoveComponent", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a2664e473a314ea56173acce95c59a56b", null ],
      [ "GenNormals", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a81a21a547e0ef83dcf980ba0aac0edd4", null ],
      [ "GenSmoothNormals", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a05479d3d9b4a2b28bf49c24a7dbcd99b", null ],
      [ "SplitLargeMeshes", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a0987753e9df81d1cf636f53c3b5593db", null ],
      [ "PreTransformVertices", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a34e3d8546cf5f6d53c7f977be8563c5f", null ],
      [ "LimitBoneWeights", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39afaf71ceef380dd2f3222f0559f5fa4e8", null ],
      [ "ValidateDataStructure", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39aea810dc92d4f56b9800d9da84c7f8b0e", null ],
      [ "ImproveCacheLocality", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39abc15722695122129538319d4fd198153", null ],
      [ "RemoveRedundantMaterials", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a220ba833233fdf0ea27c8ca100593638", null ],
      [ "FixInfacingNormals", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a66dcc1a8123e231629f8cd0470b2342f", null ],
      [ "SortByPType", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a156cb946bd7d4a5fb79cd2be00f0efcc", null ],
      [ "FindDegenerates", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39aa19a19c2bec577e10c640d67158fc602", null ],
      [ "FindInvalidData", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39aee94e10f28c445df2405ea4767905117", null ],
      [ "GenUvCoords", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39ad50e853bc8ff0eb2f56707085eb52fdb", null ],
      [ "TransformUvCoords", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a3d4d23daa2806311d58ec6b7ca15b14d", null ],
      [ "FindInstances", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39abb8af92c46a5c6bca759b86b840809e4", null ],
      [ "OptimizeMeshes", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a3d3f2effe3ccae80446075e398148e83", null ],
      [ "OptimizeGraph", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a779a78324e00278732242befd84e85be", null ],
      [ "FlipUVs", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39ad22d89e748c6ddaac37625c77d987a33", null ],
      [ "FlipWindingOrder", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a34fbdb16993386b0192a0eb0c4351c75", null ],
      [ "SplitByBoneCount", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39af2b8ea6e1c1f78bf43130045526a9f66", null ],
      [ "Debone", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a2cd14296e5ce0e8d38d42919d7ee0884", null ],
      [ "DropNormals", "_assimp_configs_8cs.html#acb0be976ed4cc12be14542c0bb29ea39a1ec0f5811ebbdbb975f9c4239b29c32c", null ]
    ] ]
];
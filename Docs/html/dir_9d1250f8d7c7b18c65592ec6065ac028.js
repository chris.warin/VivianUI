var dir_9d1250f8d7c7b18c65592ec6065ac028 =
[
    [ "BasicSample.cs", "_basic_sample_8cs.html", [
      [ "BasicSample", "class_basic_sample.html", "class_basic_sample" ]
    ] ],
    [ "CanvasSampleOpenFileImage.cs", "_canvas_sample_open_file_image_8cs.html", [
      [ "CanvasSampleOpenFileImage", "class_canvas_sample_open_file_image.html", "class_canvas_sample_open_file_image" ]
    ] ],
    [ "CanvasSampleOpenFileText.cs", "_canvas_sample_open_file_text_8cs.html", [
      [ "CanvasSampleOpenFileText", "class_canvas_sample_open_file_text.html", "class_canvas_sample_open_file_text" ]
    ] ],
    [ "CanvasSampleOpenFileTextMultiple.cs", "_canvas_sample_open_file_text_multiple_8cs.html", [
      [ "CanvasSampleOpenFileTextMultiple", "class_canvas_sample_open_file_text_multiple.html", "class_canvas_sample_open_file_text_multiple" ]
    ] ],
    [ "CanvasSampleSaveFileImage.cs", "_canvas_sample_save_file_image_8cs.html", [
      [ "CanvasSampleSaveFileImage", "class_canvas_sample_save_file_image.html", "class_canvas_sample_save_file_image" ]
    ] ],
    [ "CanvasSampleSaveFileText.cs", "_canvas_sample_save_file_text_8cs.html", [
      [ "CanvasSampleSaveFileText", "class_canvas_sample_save_file_text.html", "class_canvas_sample_save_file_text" ]
    ] ]
];
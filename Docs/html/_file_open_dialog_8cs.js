var _file_open_dialog_8cs =
[
    [ "TriLib.Samples.FileOpenDialog", "class_tri_lib_1_1_samples_1_1_file_open_dialog.html", "class_tri_lib_1_1_samples_1_1_file_open_dialog" ],
    [ "ItemType", "_file_open_dialog_8cs.html#ac95717a3db84b6b76019831e73a001c1", [
      [ "ParentDirectory", "_file_open_dialog_8cs.html#ac95717a3db84b6b76019831e73a001c1aec08dda1e93b2546dbaead35da54830a", null ],
      [ "Directory", "_file_open_dialog_8cs.html#ac95717a3db84b6b76019831e73a001c1ae73cda510e8bb947f7e61089e5581494", null ],
      [ "File", "_file_open_dialog_8cs.html#ac95717a3db84b6b76019831e73a001c1a0b27918290ff5323bea1e3b78a9cf04e", null ]
    ] ],
    [ "FileOpenEventHandle", "_file_open_dialog_8cs.html#a26c8eb38f671542450346643dc54da47", null ]
];
var dir_31956fa4d3a71221d70904ae9399c324 =
[
    [ "AssimpMetadataCollectionEditor.cs", "_assimp_metadata_collection_editor_8cs.html", [
      [ "TriLibEditor.AssimpMetadataCollectionEditor", "class_tri_lib_editor_1_1_assimp_metadata_collection_editor.html", "class_tri_lib_editor_1_1_assimp_metadata_collection_editor" ]
    ] ],
    [ "TriLibAssetEditor.cs", "_tri_lib_asset_editor_8cs.html", [
      [ "TriLibEditor.TriLibAssetEditor", "class_tri_lib_editor_1_1_tri_lib_asset_editor.html", "class_tri_lib_editor_1_1_tri_lib_asset_editor" ]
    ] ],
    [ "TriLibAssetImporter.cs", "_tri_lib_asset_importer_8cs.html", [
      [ "TriLibEditor.TriLibAssetImporter", "class_tri_lib_editor_1_1_tri_lib_asset_importer.html", "class_tri_lib_editor_1_1_tri_lib_asset_importer" ]
    ] ],
    [ "TriLibAssetPostProcessor.cs", "_tri_lib_asset_post_processor_8cs.html", [
      [ "TriLibEditor.TriLibAssetPostProcessor", "class_tri_lib_editor_1_1_tri_lib_asset_post_processor.html", "class_tri_lib_editor_1_1_tri_lib_asset_post_processor" ]
    ] ],
    [ "TriLibCheckPlugins.cs", "_tri_lib_check_plugins_8cs.html", [
      [ "TriLibEditor.TriLibCheckPlugins", "class_tri_lib_editor_1_1_tri_lib_check_plugins.html", "class_tri_lib_editor_1_1_tri_lib_check_plugins" ]
    ] ],
    [ "TriLibConfigurePlugins.cs", "_tri_lib_configure_plugins_8cs.html", [
      [ "TriLibEditor.TriLibConfigurePlugins", "class_tri_lib_editor_1_1_tri_lib_configure_plugins.html", "class_tri_lib_editor_1_1_tri_lib_configure_plugins" ]
    ] ],
    [ "TriLibDefineSymbolsHelper.cs", "_tri_lib_define_symbols_helper_8cs.html", [
      [ "TriLibEditor.TriLibDefineSymbolsHelper", "class_tri_lib_editor_1_1_tri_lib_define_symbols_helper.html", "class_tri_lib_editor_1_1_tri_lib_define_symbols_helper" ]
    ] ],
    [ "TriLibSettings.cs", "_tri_lib_settings_8cs.html", [
      [ "TriLibEditor.TriLibSettings", "class_tri_lib_editor_1_1_tri_lib_settings.html", "class_tri_lib_editor_1_1_tri_lib_settings" ]
    ] ],
    [ "TriLibSettingsProvider.cs", "_tri_lib_settings_provider_8cs.html", [
      [ "TriLibEditor.TriLibSettingsProvider", "class_tri_lib_editor_1_1_tri_lib_settings_provider.html", "class_tri_lib_editor_1_1_tri_lib_settings_provider" ]
    ] ]
];
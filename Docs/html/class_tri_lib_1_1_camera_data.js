var class_tri_lib_1_1_camera_data =
[
    [ "Aspect", "class_tri_lib_1_1_camera_data.html#abfb5a0f43fd73ada533d4d0f372ec3fa", null ],
    [ "Camera", "class_tri_lib_1_1_camera_data.html#ac5a0ab8e0d37349d707e31e5dac5b660", null ],
    [ "FarClipPlane", "class_tri_lib_1_1_camera_data.html#af4dd1246d983022597d761395598cf4e", null ],
    [ "FieldOfView", "class_tri_lib_1_1_camera_data.html#a2b684799eccc6295e86e448234d6b302", null ],
    [ "Forward", "class_tri_lib_1_1_camera_data.html#aa98f401db9e22ca9d9800f3ca0c7b5fd", null ],
    [ "LocalPosition", "class_tri_lib_1_1_camera_data.html#a76061f82ef9ca798306479dee9be39be", null ],
    [ "Name", "class_tri_lib_1_1_camera_data.html#a8c75e61de00164b45099113ad428881a", null ],
    [ "NearClipPlane", "class_tri_lib_1_1_camera_data.html#a4107efccc80b5ae3ad1136df2edd9ef1", null ],
    [ "Up", "class_tri_lib_1_1_camera_data.html#afcf44ee4de4ca869dff218bc3e95de28", null ]
];
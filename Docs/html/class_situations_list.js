var class_situations_list =
[
    [ "AddSituationUI", "class_situations_list.html#a98a8ea611b889eeecff3fbb477ad424d", null ],
    [ "AddTimeBasedTransition", "class_situations_list.html#a3c0c824ac7db1e2c74c9eda7c65dd02d", null ],
    [ "Animate", "class_situations_list.html#a6e61b5fa53ba6cac5267b8502059bbad", null ],
    [ "Animate", "class_situations_list.html#a3d97d89d55558170b29cdebf88466042", null ],
    [ "DeleteSituation", "class_situations_list.html#a80272e8d266d7a6186200bccd86394fb", null ],
    [ "MakeDefaultSituation", "class_situations_list.html#aa3a824472c1421a715e573529d48d3f2", null ],
    [ "RegisterCallbackOnSituationCreation", "class_situations_list.html#a68f87dfeacc229b7562b1c39f2b64eef", null ],
    [ "RemoveAllSituationsUI", "class_situations_list.html#ae81238927716f6041142f3f46a60d1e4", null ],
    [ "RenameSituation", "class_situations_list.html#a20be46f03502e5407e81b7120fdabbdc", null ],
    [ "contextualMenuEntries", "class_situations_list.html#a40fcd839d21eff26bf1081acb1738ed6", null ]
];
var class_tri_lib_1_1_assimp_metadata =
[
    [ "AssimpMetadata", "class_tri_lib_1_1_assimp_metadata.html#ab5b97368b37086363a8b37633810b56d", null ],
    [ "MetadataIndex", "class_tri_lib_1_1_assimp_metadata.html#a433c9de7b812a6f6abd36195145c016e", null ],
    [ "MetadataKey", "class_tri_lib_1_1_assimp_metadata.html#a3dba075c096d7f11d732be82bd6da96d", null ],
    [ "MetadataType", "class_tri_lib_1_1_assimp_metadata.html#a1bc099e82db7f45af20ede5d78dcceb9", null ],
    [ "MetadataValue", "class_tri_lib_1_1_assimp_metadata.html#ab0f4a3b1d63109a72090a90855638736", null ]
];
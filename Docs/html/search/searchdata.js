var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstuvxz",
  1: "bcdeilmnoprstuv",
  2: "mv",
  3: "bcdelmnoprstuv",
  4: "acdefgilmoprsu",
  5: "abceilmpstux",
  6: "b",
  7: "e",
  8: "bdlmnrst",
  9: "abcdeilmpstvxz"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties"
};


var class_menu =
[
    [ "AllowCollapsing", "class_menu.html#a70889c3eda88a8a8877e9701036e2187", null ],
    [ "Animate", "class_menu.html#af1ca41e00ff7be23c227d810315326ce", null ],
    [ "CreateNewProject", "class_menu.html#a8c78a03c6b59176a8d9ea93c558cbb11", null ],
    [ "ImportProject", "class_menu.html#a096f47bf482360ff81f8b1e98a9ede0a", null ],
    [ "RemoveProject", "class_menu.html#a826309695d2c6e09fddcbdd452f4ace2", null ],
    [ "RunProject", "class_menu.html#a04f13cb369d423790d48eb5b9ff97652", null ],
    [ "UpdateEntries", "class_menu.html#a70912ce01e2c369fda6f1063adae4720", null ],
    [ "contextualMainMenuEntries", "class_menu.html#a7610d77b7d1bc88ccb686a7b63693d2b", null ],
    [ "contextualProjectMenuEntries", "class_menu.html#a1e79155195f9e4a6edfb484c9e6df5e5", null ]
];
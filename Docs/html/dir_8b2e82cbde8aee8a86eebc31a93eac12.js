var dir_8b2e82cbde8aee8a86eebc31a93eac12 =
[
    [ "AnimationChannelData.cs", "_animation_channel_data_8cs.html", [
      [ "TriLib.AnimationChannelData", "class_tri_lib_1_1_animation_channel_data.html", "class_tri_lib_1_1_animation_channel_data" ]
    ] ],
    [ "AnimationCurveData.cs", "_animation_curve_data_8cs.html", [
      [ "TriLib.AnimationCurveData", "class_tri_lib_1_1_animation_curve_data.html", "class_tri_lib_1_1_animation_curve_data" ]
    ] ],
    [ "AnimationData.cs", "_animation_data_8cs.html", [
      [ "TriLib.AnimationData", "class_tri_lib_1_1_animation_data.html", "class_tri_lib_1_1_animation_data" ]
    ] ],
    [ "AssimpMetadata.cs", "_assimp_metadata_8cs.html", "_assimp_metadata_8cs" ],
    [ "CameraData.cs", "_camera_data_8cs.html", [
      [ "TriLib.CameraData", "class_tri_lib_1_1_camera_data.html", "class_tri_lib_1_1_camera_data" ]
    ] ],
    [ "ConcurrentList.cs", "_concurrent_list_8cs.html", [
      [ "TriLib.ConcurrentList< T >", "class_tri_lib_1_1_concurrent_list.html", "class_tri_lib_1_1_concurrent_list" ]
    ] ],
    [ "EmbeddedTextureData.cs", "_embedded_texture_data_8cs.html", "_embedded_texture_data_8cs" ],
    [ "FileLoadData.cs", "_file_load_data_8cs.html", [
      [ "TriLib.BrowserLoadData", "class_tri_lib_1_1_browser_load_data.html", "class_tri_lib_1_1_browser_load_data" ],
      [ "TriLib.GCFileLoadData", "class_tri_lib_1_1_g_c_file_load_data.html", "class_tri_lib_1_1_g_c_file_load_data" ],
      [ "TriLib.FileLoadData", "class_tri_lib_1_1_file_load_data.html", "class_tri_lib_1_1_file_load_data" ]
    ] ],
    [ "MaterialData.cs", "_material_data_8cs.html", "_material_data_8cs" ],
    [ "MeshData.cs", "_mesh_data_8cs.html", [
      [ "TriLib.MeshData", "class_tri_lib_1_1_mesh_data.html", "class_tri_lib_1_1_mesh_data" ]
    ] ],
    [ "MorphChannelData.cs", "_morph_channel_data_8cs.html", [
      [ "TriLib.MorphChannelData", "class_tri_lib_1_1_morph_channel_data.html", "class_tri_lib_1_1_morph_channel_data" ]
    ] ],
    [ "MorphChannelKey.cs", "_morph_channel_key_8cs.html", [
      [ "TriLib.MorphChannelKey", "class_tri_lib_1_1_morph_channel_key.html", "class_tri_lib_1_1_morph_channel_key" ]
    ] ],
    [ "MorphData.cs", "_morph_data_8cs.html", [
      [ "TriLib.MorphData", "class_tri_lib_1_1_morph_data.html", "class_tri_lib_1_1_morph_data" ]
    ] ],
    [ "NodeData.cs", "_node_data_8cs.html", [
      [ "TriLib.NodeData", "class_tri_lib_1_1_node_data.html", "class_tri_lib_1_1_node_data" ]
    ] ]
];
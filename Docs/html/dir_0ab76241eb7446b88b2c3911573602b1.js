var dir_0ab76241eb7446b88b2c3911573602b1 =
[
    [ "Sample", "dir_9d1250f8d7c7b18c65592ec6065ac028.html", "dir_9d1250f8d7c7b18c65592ec6065ac028" ],
    [ "IStandaloneFileBrowser.cs", "_i_standalone_file_browser_8cs.html", [
      [ "SFB.IStandaloneFileBrowser", "interface_s_f_b_1_1_i_standalone_file_browser.html", "interface_s_f_b_1_1_i_standalone_file_browser" ]
    ] ],
    [ "StandaloneFileBrowser.cs", "_standalone_file_browser_8cs.html", [
      [ "SFB.ExtensionFilter", "struct_s_f_b_1_1_extension_filter.html", "struct_s_f_b_1_1_extension_filter" ],
      [ "SFB.StandaloneFileBrowser", "class_s_f_b_1_1_standalone_file_browser.html", "class_s_f_b_1_1_standalone_file_browser" ]
    ] ],
    [ "StandaloneFileBrowserEditor.cs", "_standalone_file_browser_editor_8cs.html", null ],
    [ "StandaloneFileBrowserLinux.cs", "_standalone_file_browser_linux_8cs.html", null ],
    [ "StandaloneFileBrowserMac.cs", "_standalone_file_browser_mac_8cs.html", null ],
    [ "StandaloneFileBrowserWindows.cs", "_standalone_file_browser_windows_8cs.html", null ]
];
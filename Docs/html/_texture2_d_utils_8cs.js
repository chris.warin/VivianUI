var _texture2_d_utils_8cs =
[
    [ "TriLib.Texture2DUtils", "class_tri_lib_1_1_texture2_d_utils.html", "class_tri_lib_1_1_texture2_d_utils" ],
    [ "TextureCompression", "_texture2_d_utils_8cs.html#a6ab41fbb21f3635083be3b1178394d0c", [
      [ "None", "_texture2_d_utils_8cs.html#a6ab41fbb21f3635083be3b1178394d0ca6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "NormalQuality", "_texture2_d_utils_8cs.html#a6ab41fbb21f3635083be3b1178394d0caa7e70da92c2e5a1c4eaf14cca2a63dcd", null ],
      [ "HighQuality", "_texture2_d_utils_8cs.html#a6ab41fbb21f3635083be3b1178394d0ca175583ddd7989a8c2807a52cde97bd19", null ]
    ] ],
    [ "TextureLoadHandle", "_texture2_d_utils_8cs.html#aa702927d234b3c06824161ecd963fbc7", null ],
    [ "TexturePreLoadHandle", "_texture2_d_utils_8cs.html#ac56fe0608cd21923e33256816495b4f5", null ]
];
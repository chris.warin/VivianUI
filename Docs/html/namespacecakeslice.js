var namespacecakeslice =
[
    [ "LinkedSet", "classcakeslice_1_1_linked_set.html", "classcakeslice_1_1_linked_set" ],
    [ "MaterialSwitcher", "classcakeslice_1_1_material_switcher.html", "classcakeslice_1_1_material_switcher" ],
    [ "Outline", "classcakeslice_1_1_outline.html", "classcakeslice_1_1_outline" ],
    [ "OutlineAnimation", "classcakeslice_1_1_outline_animation.html", null ],
    [ "OutlineEffect", "classcakeslice_1_1_outline_effect.html", "classcakeslice_1_1_outline_effect" ],
    [ "Rotate", "classcakeslice_1_1_rotate.html", null ],
    [ "Toggle", "classcakeslice_1_1_toggle.html", null ]
];
var class_tri_lib_1_1_animation_data =
[
    [ "AnimationClip", "class_tri_lib_1_1_animation_data.html#a2de514f0b8d21a7c19b709745ce987d9", null ],
    [ "ChannelData", "class_tri_lib_1_1_animation_data.html#a72ba37aa36e550d9b4b466cab2e7f96c", null ],
    [ "FrameRate", "class_tri_lib_1_1_animation_data.html#abaa655abeb671ecb41566d441da36e2c", null ],
    [ "Legacy", "class_tri_lib_1_1_animation_data.html#a719a326019c68748b48e8726c9ad2eab", null ],
    [ "Length", "class_tri_lib_1_1_animation_data.html#a347e61fb9490f5ad3555f8cd1c479f1d", null ],
    [ "MorphData", "class_tri_lib_1_1_animation_data.html#a719dc5ee89b5a65ed9f204ace510872e", null ],
    [ "Name", "class_tri_lib_1_1_animation_data.html#ae507b7dc8c52e9be2fcf93454f0f62d1", null ],
    [ "WrapMode", "class_tri_lib_1_1_animation_data.html#a64c94d802d7ca4ba1f3920ef95446d9b", null ]
];
var namespace_tri_lib_1_1_extras =
[
    [ "AvatarLoader", "class_tri_lib_1_1_extras_1_1_avatar_loader.html", "class_tri_lib_1_1_extras_1_1_avatar_loader" ],
    [ "AvatarLoaderSample", "class_tri_lib_1_1_extras_1_1_avatar_loader_sample.html", "class_tri_lib_1_1_extras_1_1_avatar_loader_sample" ],
    [ "BoneRelationship", "class_tri_lib_1_1_extras_1_1_bone_relationship.html", "class_tri_lib_1_1_extras_1_1_bone_relationship" ],
    [ "BoneRelationshipList", "class_tri_lib_1_1_extras_1_1_bone_relationship_list.html", "class_tri_lib_1_1_extras_1_1_bone_relationship_list" ]
];
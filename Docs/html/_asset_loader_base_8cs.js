var _asset_loader_base_8cs =
[
    [ "TriLib.AssetLoaderBase", "class_tri_lib_1_1_asset_loader_base.html", "class_tri_lib_1_1_asset_loader_base" ],
    [ "AnimationClipCreatedHandle", "_asset_loader_base_8cs.html#a2087da18b5322c2c97a441cab1a453a9", null ],
    [ "AvatarCreatedHandle", "_asset_loader_base_8cs.html#a5685574af3f428ce46cc049f6c002406", null ],
    [ "BlendShapeKeyCreatedHandle", "_asset_loader_base_8cs.html#ac4e55754b655cc8a70d70c7b0f4bdef5", null ],
    [ "EmbeddedTextureLoadCallback", "_asset_loader_base_8cs.html#a9c9419cd89ec12f9954438305c039c44", null ],
    [ "LoadTextureDataCallback", "_asset_loader_base_8cs.html#a5a26f74a4ebcff8138135eac4633ac33", null ],
    [ "MaterialCreatedHandle", "_asset_loader_base_8cs.html#a82fca2459006c57cd350c342fc1fa3ce", null ],
    [ "MeshCreatedHandle", "_asset_loader_base_8cs.html#aa973ed297057ea84004ae8b75b368d29", null ],
    [ "MetadataProcessedHandle", "_asset_loader_base_8cs.html#aa3525ab1c9d6ce079a8e0626eb444d6a", null ],
    [ "ObjectLoadedHandle", "_asset_loader_base_8cs.html#a9daf3939b26812f1c6b8ce45223cebde", null ]
];
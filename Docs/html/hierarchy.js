var hierarchy =
[
    [ "ContextualMenuEntries", "struct_contextual_menu_entries.html", null ],
    [ "ExtensionMethods", "class_extension_methods.html", null ],
    [ "IDisposable", null, [
      [ "Controls", "class_controls.html", null ]
    ] ],
    [ "IInputActionCollection2", null, [
      [ "Controls", "class_controls.html", null ]
    ] ],
    [ "INotifyCompletion", null, [
      [ "UnityWebRequestAwaiter", "struct_unity_web_request_awaiter.html", null ]
    ] ],
    [ "InspectorSpecsUI", "struct_inspector_specs_u_i.html", null ],
    [ "Controls.IPlayerActions", "interface_controls_1_1_i_player_actions.html", null ],
    [ "MonoBehaviour", null, [
      [ "ElementPicker", "class_element_picker.html", null ],
      [ "Menu", "class_menu.html", null ],
      [ "Modal", "class_modal.html", [
        [ "Modals.CreateProjectModal", "class_modals_1_1_create_project_modal.html", null ]
      ] ],
      [ "ModelManipulator", "class_model_manipulator.html", null ],
      [ "Navbar", "class_navbar.html", null ],
      [ "Outliner", "class_outliner.html", null ],
      [ "ProjectFilesController", "class_project_files_controller.html", null ],
      [ "SituationsList", "class_situations_list.html", null ],
      [ "VivianComponents.VivianComponent", "class_vivian_components_1_1_vivian_component.html", [
        [ "VivianComponents.Button", "class_vivian_components_1_1_button.html", null ],
        [ "VivianComponents.Display", "class_vivian_components_1_1_display.html", null ],
        [ "VivianComponents.Light", "class_vivian_components_1_1_light.html", null ],
        [ "VivianComponents.Movable", "class_vivian_components_1_1_movable.html", null ],
        [ "VivianComponents.Rotatable", "class_vivian_components_1_1_rotatable.html", null ],
        [ "VivianComponents.Slider", "class_vivian_components_1_1_slider.html", null ],
        [ "VivianComponents.Sound", "class_vivian_components_1_1_sound.html", null ],
        [ "VivianComponents.Text", "class_vivian_components_1_1_text.html", null ],
        [ "VivianComponents.Touchscreen", "class_vivian_components_1_1_touchscreen.html", null ]
      ] ],
      [ "VivianUI", "class_vivian_u_i.html", null ]
    ] ],
    [ "Controls.PlayerActions", "struct_controls_1_1_player_actions.html", null ],
    [ "ProjectJsonWrapper", "class_project_json_wrapper.html", null ],
    [ "ProjectsJsonWrapper", "class_projects_json_wrapper.html", null ],
    [ "ScriptableObject", null, [
      [ "ElementVivianSpecs", "class_element_vivian_specs.html", null ],
      [ "Utils", "class_utils.html", null ]
    ] ]
];
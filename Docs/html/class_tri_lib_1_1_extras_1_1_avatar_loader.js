var class_tri_lib_1_1_extras_1_1_avatar_loader =
[
    [ "LoadAvatar", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#ae0f744dc362b0523c417abd81e908c17", null ],
    [ "LoadAvatarFromMemory", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a71953f37279d2c1960e0fb1a8e51297d", null ],
    [ "Start", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a6f3bd9cfc98d9642f467918d5184c494", null ],
    [ "ArmStretch", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#ab06db973309cf4c51111cf208a361042", null ],
    [ "CurrentAvatar", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a737b8b1f730a32ae48c05cd393b2092c", null ],
    [ "CustomBoneNames", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a23e3c9e83fc9dcb5c505dc20400fb03e", null ],
    [ "FeetSpacing", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a39147692e7d1f6e75b82e82fb9701f44", null ],
    [ "HasTranslationDof", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a024ddc37f4f364cb8e9bde4b35a04275", null ],
    [ "HeightOffset", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a98d1e479dbbd3b22ef91e26c9050975b", null ],
    [ "LegStretch", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a06e71fa812bf44caab6d867df456ec54", null ],
    [ "LowerArmTwist", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a35e4616ffc830804abaef25c4827bb48", null ],
    [ "LowerLegTwist", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a541804976529f38a90c182c273a2755a", null ],
    [ "RuntimeAnimatorController", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#add755bd690c0030808ad230041e4a8fd", null ],
    [ "Scale", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#ae40cbb03909baa43f0e3888ad23a563b", null ],
    [ "UpperArmTwist", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#ac4ffb3172579da4596b4dabaf1149c73", null ],
    [ "UpperLegTwist", "class_tri_lib_1_1_extras_1_1_avatar_loader.html#a69b4be6c4355eb2136fb0d1c7a1e412e", null ]
];
# VivianUI
VivianUI is a GUI cross-platform application for the [Vivian Framework](https://gitlab.com/usability-engineering-ar-mr-vr/vivian-framework/vivian-core). It is the natural continuation of [Vivifly](https://gitlab.com/usability-engineering-ar-mr-vr/vivian-ui/vivifly), and is similar to it. It is however a complete reimplementation, made purely with Unity and the [XRUI framework](https://github.com/chwar/XRUI).

VivianUI is meant to be cross-platform for mobile AR (Android, iOS) and VR (Windows). 

## Project initialization
1. When cloning the project, make sure to initialize the git submodules.
- In a GUI client, this can be proposed to you when cloning the project.
- If you use Git through the CLI, run this command to have the submodules directly checked out:
`git clone --recurse-submodules https://gitlab.gwdg.de/chris.warin/VivianUI.git`
- If you've already checked out the project: `git submodule init && git submodule update`

2. Open the project from UnityHub with the correct version (currently Unity 2021.2.5f1). This is important, as XRUI is only compatible with Unity 2021.2+. 

3. Import TriLib from your Unity account. This is a paid extention which can obviously not be versioned in the git repository for copyright reasons. VivianUI will not function without it.  

## Roadmap
The remaining implementation is listed in the [issues list](https://gitlab.gwdg.de/chris.warin/VivianUI/-/issues?scope=all&state=opened&label_name[]=implementation).
